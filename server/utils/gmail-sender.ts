const functions = require('firebase-functions');
const nodemailer = require('nodemailer');

const gmailEmail = functions.config().gmail ? functions.config().gmail.email : '';
const gmailPassword = functions.config().gmail ? functions.config().gmail.password : '';
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword
  }
});

// Sends a welcome email to the given user.
export function sendEmailWithGMAIL(fromName, fromEmail, email, displayName, subject, body) {
  let mailOptions: any = {
    from: `${fromName} <${fromEmail}>`,
    to: email
  };

  // The user subscribed to the newsletter.
  mailOptions.subject = subject;
  mailOptions.text = body;
  return mailTransport.sendMail(mailOptions).then(() => {
    console.log('Email sent to:', email);
  });
};