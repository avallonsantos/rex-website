import 'zone.js/dist/zone-node';
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';
import { renderModuleFactory } from '@angular/platform-server';
import * as fs from 'fs'

const { firebaseAuthServer } = require('./default-server/firebase-auth-server');
admin.initializeApp(functions.config().firebase);

import { breedsProvider } from './custom-server/breeds'
import { statisticProvider } from './custom-server/statistics'
import { ratingsProvider } from './custom-server/ratings'

const APP_NAME = "Rex";
const APP_EMAIL = "rexbichos@gmail.com";
import { sendEmailWithGMAIL } from './utils/gmail-sender'

// WEBSITE __START
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';  
const document = fs.readFileSync(__dirname + '/dist-server/index.html', 'utf8'); 
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./dist-server/main.bundle'); 

import { UniversalServer } from './custom-server/universal-server'
const website = new UniversalServer();
website.renderHTMLCallback = (url, defaultProviders) => {
    // const timeStart = new Date();
    const extraProviders = [
        provideModuleMap(LAZY_MODULE_MAP)
    ];
    for (let index = 0; index < defaultProviders.length; index++) {
        console.log('extraProvider', defaultProviders[index].provide);
        console.log('extraProvider data', defaultProviders[index].useValue);
        extraProviders.push(defaultProviders[index]);
    }
    return renderModuleFactory(AppServerModuleNgFactory, { document: document, url, extraProviders });
};
website.initWebsite(exports);
// WEBSITE __END

// TRIGGERS __START
exports.onUserSaved = functions.firestore.document('users/{userId}').onUpdate(event => {
    var userId = event.params.userId;
    var user = event.data.data();
    var itemRef = event.data.ref;

    // itemRef.update({ status: "VALUE" }).then(() => { });
    // admin.firestore().collection('pets').doc("ID").get();
    return true;
});

exports.onPlaceSaved = functions.firestore.document('places/{placeId}').onUpdate(event => {
    var placeId = event.params.placeId;
    var place = event.data.data();
    var itemRef = event.data.ref;

    return true;
});

exports.onPetSaved = functions.firestore.document('pets/{petId}').onUpdate(event => {
    var petId = event.params.petId;
    var pet = event.data.data();
    var itemRef = event.data.ref;

    const promises = [];
    if (event.data.previous.exists) {
        var previousPet = event.data.previous.data();
        if (previousPet.type) {
            if (previousPet.type != pet.type) {
                promises.push(breedsProvider.checkPetBreed(previousPet.type, null, previousPet.breed));
                if (pet.type)
                    promises.push(breedsProvider.checkPetBreed(pet.type, pet.breed, null));
            } else
                promises.push(breedsProvider.checkPetBreed(pet.type, pet.breed, previousPet.breed));
        } else if (pet.type)
            promises.push(breedsProvider.checkPetBreed(pet.type, pet.breed, null));

    } else if (pet.type) {
        promises.push(breedsProvider.checkPetBreed(pet.type, pet.breed, null));
    }

    if (promises.length > 0) 
        return Promise.all(promises);
    else
        return true;
});
exports.onPetRemoved = functions.firestore.document('pets/{petId}').onDelete(event => {
    var petId = event.params.petId;
    var pet = event.data.previous.data();

    return admin.firestore().collection('interests').where('pet', "==", `pets/${petId}`).get().then(interestsSnaps => {
        const promises = [ statisticProvider.removeAllPetStatistics(pet, petId) ];
        if (pet.type && pet.breed) {
            promises.push(breedsProvider.checkPetBreed(pet.type, null, pet.breed));
        }
        interestsSnaps.forEach(interestSnap => {
            promises.push(interestSnap.ref.delete())
        });
        return Promise.all(promises);
    });
});

exports.onInterestCreated = functions.firestore.document('interests/{interestId}').onCreate(event => {
    var interestId = event.params.interestId;
    var interest = event.data.data();
    var itemRef = event.data.ref;
    if (interest && interest.type && interest.pet) {
        return new Promise((resolve, reject) => {
            return statisticProvider.increasePetStatistic(interest.pet.id, interest.type).then(statResult => {
                Promise.all([interest.pet.get(), interest.user.get()]).then(results => {
                    let petId = results[0].id;
                    let pet = results[0].data();
                    let userId = results[1].id;
                    let user = results[1].data();

                    const subject = `Novo Interesse: ${interest.type}`
                    const body = `Um usuário marcou ter interesse em:
                    \r\n\r\n
                    *Interesse*\r\n
                    Tipo: ${interest.type}\r\n
                    Data: ${interest.createdAt}\r\n
                    \r\n\r\n
                    *Pet*\r\n
                    Nome: ${pet.name}\r\n
                    Tipo: ${pet.type}\r\n
                    Página: http://rex.com.br/pet/${pet.slug || pet.id}\r\n
                    \r\n\r\n
                    *Usuário*\r\n
                    Nome: ${user.displayName}\r\n
                    Email: ${user.email}\r\n
                    ID: ${userId}
                    `
                    
                    sendEmailWithGMAIL(APP_NAME, APP_EMAIL, APP_EMAIL, APP_NAME, subject, body).then(() => {
                        resolve(statResult);
                    }, error => {
                        resolve(statResult);
                    })
                }, error => {
                    resolve(statResult);
                });
            }, reject);
        })
        
    } else {
        return true;
    }
});
exports.onInterestUpdated = functions.firestore.document('interests/{interestId}').onUpdate(event => {
    var interestId = event.params.interestId;
    var interest = event.data.data();
    var itemRef = event.data.ref;

    var previousInterest = null;
    if (event.data.previous.exists) {
        previousInterest = event.data.previous.data();
    }

    return ratingsProvider.checkInterestRating(interest, previousInterest);
});
// TRIGGERS __END

// PUBLIC HTTP __START
const publicApp = firebaseAuthServer.setupOptional();
publicApp.post('/pet/visit', (req, res) => {
    statisticProvider.increasePetStatistic(req.body.petId, "visit").then(result => {
        res.status(200).send({ success: true });
    }, error => {
        res.status(500).send({ error: error });
    });
});
publicApp.post('/contact', (req, res) => {
    let message = req.body.message;
    let userSubject = req.body.subject;
    let email = req.body.email;
    let displayName = req.body.displayName;
    if (req.user) {
        if (req.email) email = req.user.email;
        if (req.displayName) displayName = req.user.displayName;
    }

    let errors: any = { };
    if (!email || email.length == 0) errors.email = "Email precisa ser preenchido"
    if (!displayName || displayName.length == 0) errors.displayName = "Nome precisa ser preenchido";
    if (!message || message.length == 0) errors.message = "Mensagem precisa ser preenchida";
    if (!userSubject || userSubject.length == 0) errors.subject = "Assunto precisa ser preenchido";

    if (Object.keys(errors).length == 0) {
        let subject = `Contato Rex: ${userSubject}`;
        let body = `Foi enviado um contato pelo site:\r\n\r\n
        Nome:${displayName}\r\n
        E-mail:${email}\r\n
        Assunto:${userSubject}\r\n
        Mensagem:\r\n
        ${message}`

        try {
            sendEmailWithGMAIL(APP_NAME, APP_EMAIL, APP_EMAIL, APP_NAME, subject, body).then(() => {
                res.status(200).send({ success: true });
            }, error => {
                res.status(500).send({ error: error });
            });
        } catch (ex) {
            res.status(500).send({ error: ex });    
        }
    } else {
        res.status(422).send({ error: errors });
    }
});
publicApp.get('/pet/reloadUsedBreeds', (req, res) => {
    breedsProvider.initBreeds().then(result => {
        res.status(200).send({ success: true });
    }, error => {
        res.status(500).send({ error: error });
    });
});
exports.pub = functions.https.onRequest(publicApp);
// PUBLIC HTTP __END