import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import { slugify } from '../utils/string'

function initBreeds() {
  return new Promise((resolve, reject) => {
      const reImportBreeds = false;
      let dogBreeds = null;
      let catBreeds = null;
      let dogPromise = null;
      let catPromise = null;
      if (reImportBreeds) {
          dogBreeds = ["Afegão Hound", "Affenpinscher", "Airedale Terrier", "Akita", "American Staffordshire Terrier", "Australian Cattle Dog (Boiadero Australiano)", "Basenji", "Basset Hound", "Beagle", "Beagle Harrier", "Bearded Collie", "Bedlington Terrier", "Bernese Mountain Dog (Boiadero Bernês)", "Bichon Frisé", "Bloodhound", "Bobtail", "Boder Collie", "Border Terrier", "Borzoi", "Boston Terrier", "Boxer", "Buldogue Francês", "Buldogue Inglês", "Bull Terrier", "Bulmastife", "Cairn Terrier", "Cane Corso", "Cão de Água Português", "Cão de Crista Chinês", "Cavalier King Charles Spaniel", "Chesapeake Bay Retriever", "Chihuahua", "Chow Chow", "Cocker Spaniel Americano", "Cocker Spaniel Inglês", "Collie", "Coton de Tuléar", "Dachshund (Salsicha)", "Dálmata", "Dandie Dinmont Terrier", "Doberman", "Dogo Argentino", "Dogue Alemão", "Fila Brasileira", "Fox Terrier (Pelo Liso ou Pelo Curto)", "Foxhound Inglês", "Galgo Escocês", "Galgo Irlandês", "Golden Retriever", "Grande Boiadeiro Suiço", "Greyhound", "Grifo da Bélgica", "Husky Siberiano", "Jack Russell Terrier", "King Charles", "Komondor", "Labradoodle ", "Labrador Retriever", "Lakeland Retriever", "Leonberger", "Lhasa Apso", "Malamute do Alasca", "Maltês", "Mastife", "Mastim Napolitano", "Mastim Tibetano", "Norfolk Terrier", "Norwich Terrier", "Papillon", "Pastor Alemão ", "Pastor Australino ", "Pinscher Miniatura", "Poodle ", "Pug", "Rottweiler", "Sem Raça Definida (SRD)", "Shihtzu", "Silky Terrier", "Skye Terrier", "Spitz Alemão Anão (Lulu da Pomerânia)", "Staffordshire Bull Terrier", "Terra Nova", "Terrier Escocês", "Tosa", "Weimaraner", "Welsh Corgi (Cardigan)", "Welsh Corgi (Pembroke)", "West Highland White Terrier", "Whippet", "Xoloitzcuintli", "Yorkshire Terrier"];
          catBreeds = ["Abissinio", "American Shorthair", "Angora", "Azul Russo", "Bengal", "Brazilian Shorthair", "Himalaia", "Persa", "Sem Raça Definida (SRD)", "Siâmes"];
          dogPromise = admin.firestore().doc(`pet_types/dog`).update({ breeds: dogBreeds });
          catPromise = admin.firestore().doc(`pet_types/cat`).update({ breeds: catBreeds });
      } else {
          dogPromise = admin.firestore().doc(`pet_types/dog`).get();
          catPromise = admin.firestore().doc(`pet_types/cat`).get();
      }
      
      Promise.all([dogPromise, catPromise]).then(breedResults => {
          if (!dogBreeds) {
              dogBreeds = breedResults[0].data().breeds;
          }
          if (!catBreeds) {
              catBreeds = breedResults[1].data().breeds;
          }
      
          admin.firestore().collection("pets").get().then(itemsSnap => {

              const promises = [];
              itemsSnap.forEach(itemSnap => {
                  let petId = itemSnap.id;
                  let pet = itemSnap.data();
                  if (!pet.breed || pet.breed == "Vira-Lata" || pet.breed.length == 0 || (dogBreeds.indexOf(pet.breed) == -1 && catBreeds.indexOf(pet.breed) == -1)) {
                      promises.push(admin.firestore().doc(`pets/${petId}`).update({ breed: "Sem Raça Definida (SRD)" }))
                  }
              })

              Promise.all(promises).then(results => {
                  Promise.all([checkAllBreeds("dog"), checkAllBreeds("cat")]).then(resolve, reject);
              }, reject);
          }, reject)
      }, reject)
  });
}

function checkPetBreed(petType, breed, previousBreed) {
  return new Promise((resolve, reject) => {
      let petTypeRef = admin.firestore().doc(`pet_types/${petType}`);
      petTypeRef.get().then(petTypeSnap => {
          if (petTypeSnap.exists) {
              let petTypeData = petTypeSnap.data();
              if (previousBreed) {
                  const previousBreedSlug = slugify(previousBreed);
                  const previousBreedCount = petTypeData.usedBreedsMap[previousBreedSlug];
                  if (!previousBreedCount || previousBreedCount < 2) {
                      delete petTypeData.usedBreedsMap[previousBreedSlug];
                      let indexOfList = petTypeData.usedBreeds.indexOf(previousBreed);
                      if (indexOfList > -1)
                          petTypeData.usedBreeds.splice( indexOfList, 1 )
                  } else {
                      petTypeData.usedBreedsMap[previousBreedSlug] = previousBreedCount - 1;
                  }
              }

              if (breed) {
                  const breedSlug = slugify(breed);
                  const breedCount = petTypeData.usedBreedsMap[breedSlug];
                  if (!breedCount) {
                      petTypeData.usedBreedsMap[breedSlug] = 1;
                      let indexOfList = petTypeData.usedBreeds.indexOf(breed);
                      if (indexOfList == -1)
                          petTypeData.usedBreeds.push( breed )
                  } else {
                      petTypeData.usedBreedsMap[breedSlug] = breedCount + 1;
                  }
              }


              petTypeRef.update(petTypeData).then(resolve, reject);
          }
      }, reject);
      
  });
}

function checkAllBreeds(petType) {
  return new Promise((resolve, reject) => {
      const breedHash = { };
      const breeds = [];
      admin.firestore().collection("pets").where("type", "==", petType).get().then(itemsSnap => {
        itemsSnap.forEach(itemSnap => {
          let petId = itemSnap.id;
          let pet = itemSnap.data();
          let breed = pet.breed;
          if (!pet.breed || pet.breed == "Vira-Lata" || pet.breed.length == 0) {
              pet.breed = "Sem Raça Definida (SRD)";
          }

          const slug = slugify(pet.breed);
          if (breedHash.hasOwnProperty(slug)) {
              breedHash[slug] = breedHash[slug] + 1;
          } else {
              breedHash[slug] = 1;
              breeds.push(pet.breed);
          }
        });

        admin.firestore().doc(`pet_types/${petType}`).update({ usedBreeds: breeds, usedBreedsMap: breedHash }).then(resolve, reject);
      });
  });
}

export const breedsProvider = {
  initBreeds: initBreeds,
  checkPetBreed: checkPetBreed,
  checkAllBreeds: checkAllBreeds,
};