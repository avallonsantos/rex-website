import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

function increasePetStatistic(petId, statProperty) {
  return new Promise((resolve, reject) => {
    admin.firestore().collection('pets').doc(petId).get().then(petResult => {
      const petData = petResult.exists ? petResult.data() : null;
      if (petData) {
        const promises = [];
        promises.push(doIncreasePetStatistic(petId, statProperty));
        if (petData.user) {
          promises.push(doIncreasePetOwnerStatistic(`usersStats/${petData.user.id}`, petId, statProperty));
        }
        if (petData.place) {
          promises.push(doIncreasePetOwnerStatistic(`placesStats/${petData.place.id}`, petId, statProperty));
        }
        if (promises.length > 0) {
          Promise.all(promises).then(resolve, reject);
        } else {
          resolve(null);
        }
      } else {
        resolve(null);
      }
    }, reject)
  });
};

function removeAllPetStatistics(petData, petId) {
  return new Promise((resolve, reject) => {
    const promises = [];
    promises.push(doRemoveAllPetStatistic(petId));
    if (petData.user) {
      promises.push(doRemoveAllPetOwnerStatistic(`usersStats/${petData.user.id}`, petId));
    }
    if (petData.place) {
      promises.push(doRemoveAllPetOwnerStatistic(`placesStats/${petData.place.id}`, petId));
    }
    if (promises.length > 0) {
      Promise.all(promises).then(resolve, reject);
    } else {
      resolve(null);
    }
  });
}

function doRemoveAllPetStatistic(petId) {
  return new Promise((resolve, reject) => {
    const documentRef = admin.firestore().doc(`petsStats/${petId}`);
    documentRef.delete().then(resolve, reject)
  })
};

function doRemoveAllPetOwnerStatistic(docPath, petId) {
  return new Promise((resolve, reject) => {
    const documentRef = admin.firestore().doc(docPath);
    documentRef.get().then(docResult => {
      let stats = docResult.exists ? docResult.data() : null;
      if (stats && stats.pets && stats.pets[petId]) {
        delete stats.pets[petId];
        if (Object.keys(stats.pets).length == 0) {
          delete stats["pets"];
        }
        if (Object.keys(stats).length == 0) {
          documentRef.delete().then(resolve, reject);
        } else {
          documentRef.set(stats).then(resolve, reject);
        }
      } else {
        resolve(true);
      }
    }, reject);
  })
};

function doIncreasePetStatistic(petId, statProperty) {
  return new Promise((resolve, reject) => {
    const documentRef = admin.firestore().doc(`petsStats/${petId}`);
    documentRef.get().then(docResult => {
      let currentPet = docResult.exists ? docResult.data() : { };
      let count = currentPet[statProperty];
      if (count)
        currentPet[statProperty] = count + 1;
      else
        currentPet[statProperty] = 1;
      documentRef.set(currentPet).then(resolve, reject);
    }, reject)
  })
};

function doIncreasePetOwnerStatistic(docPath, petId, statProperty) {
  return new Promise((resolve, reject) => {
    const documentRef = admin.firestore().doc(docPath);
    documentRef.get().then(docResult => {
      let stats = docResult.exists ? docResult.data() : null;
      if (stats) {
        if (!stats.pets)
          stats.pets = { };
      } else {
        stats = { pets: { } };
      }
      let currentPet = stats.pets[petId];
      if (currentPet) {
        let count = currentPet[statProperty];
        if (count)
          currentPet[statProperty] = count + 1;
        else
          currentPet[statProperty] = 1;
      } else {
        currentPet = { };
        currentPet[statProperty] = 1;
        stats.pets[petId] = currentPet;
      }
      documentRef.set(stats).then(resolve, reject);
    }, reject);
  })
};

export const statisticProvider = {
  increasePetStatistic: increasePetStatistic,
  removeAllPetStatistics: removeAllPetStatistics
}