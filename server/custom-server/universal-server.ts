import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as express from 'express';

import { getIdFromSlugified, slugifyWithId } from '../utils/string'

const Cache = require('caching-map');
const ms = require('ms');

export class UniversalServer {
  CACHED_DOCS = new Cache(Infinity);
  PAGES_CACHED_DOCS = new Cache(40);
  mainPagesTtl = ms('1h');
  placeCache = new Cache(Infinity);
  petCache = new Cache(Infinity);
  ttl = ms('5m');

  renderHTMLCallback: (url: string, defaultProviders:any[]) => any;

  constructor() {
    const websiteInstance = this;

    this.placeCache.materialize = function(slug) {
      const promise = websiteInstance.getFromSlug('places', slug);
  
      promise.then(item => {
          websiteInstance.placeCache.set(slug, item, { ttl: websiteInstance.ttl });
      });
      return promise;
    }
    this.petCache.materialize = function(slug) {
        const promise = websiteInstance.getFromSlug('pets', slug);
    
        promise.then(item => {
          websiteInstance.petCache.set(slug, item, { ttl: websiteInstance.ttl });
        });
        return promise;
    }
  }

  addHeadersToResult(res) {
    // res.set('Cache-Control', 'public, max-age=600, s-maxage=1200');
  }
  
  renderAndCache(url, defaultProviders = [], cache = null, ttl = null) {
    const websiteInstance = this;

    return this.renderHTMLCallback(url, defaultProviders)
      .then(html => {
        let cacheMap = cache ? cache : websiteInstance.CACHED_DOCS;
        cacheMap.set(url, html, { ttl });
        // console.log('cached time needed (s) ' + url, ((new Date().getTime() - timeStart.getTime()) / 1000));
        return html;
    });
  }

  renderAndCacheWithData(url, dataName, data, cache = null, ttl = null) {
    return this.renderAndCache(url, [{ provide: dataName, useValue: data, deps: [] },], cache, ttl);
  }

  renderFindFriend() {
    const websiteInstance = this;

    admin.firestore().collection("pets").orderBy("bornAt").limit(20).get().then(itemsSnap => {
      var pets = [];
      itemsSnap.forEach(itemSnap => {
        var pet = itemSnap.data();
        pet.id = itemSnap.id;
        pets.push(pet);
        websiteInstance.petCache.set(pet.slug || slugifyWithId(pet.name, pet.id), pet, { ttl: websiteInstance.ttl });
      });
      websiteInstance.renderAndCacheWithData("/encontrar-melhor-amigo", "pets", pets, null, websiteInstance.mainPagesTtl);
    }, error => {
      websiteInstance.renderAndCache("/encontrar-melhor-amigo", [], null, websiteInstance.mainPagesTtl);
    });
  }

  initWebsite(mainExports) {
    const websiteInstance = this;

    websiteInstance.renderAndCache("");
    websiteInstance.renderAndCache("/");
    websiteInstance.renderAndCache("/home");
    websiteInstance.renderFindFriend();
    websiteInstance.renderAndCache("/registrar");
    websiteInstance.renderAndCache("/registrar-ong");
    websiteInstance.renderAndCache("/registrar-canil");

    const timeout = require('connect-timeout');

    const app = express();
    // app.use(timeout(300000));
    app.get('/pet/:slug', (req, res) => {
        console.log('/pet/:slug');
        websiteInstance.processItemRequest('pet', websiteInstance.petCache, req, res)
    });
    app.get('/place/:slug', (req, res) => {
        console.log('/place/:slug');
        websiteInstance.processItemRequest('place', websiteInstance.placeCache, req, res)
    });
    app.get('**', (req, res) => {
        const url = req.path;
        let html = websiteInstance.CACHED_DOCS.get(url) || websiteInstance.PAGES_CACHED_DOCS.get(url);
        if (html) {
            this.addHeadersToResult(res);
            res.send(html);
        } else {
            const date = new Date();
            websiteInstance.renderAndCache(url, [], websiteInstance.PAGES_CACHED_DOCS).then(html => {
                this.addHeadersToResult(res);
                console.log('loaded in', (((new Date().getTime()) - date.getTime()) / 1000));
                res.send(html);
            });
        }
    });

    mainExports.ssrapp = functions.https.onRequest(app);
  }

  promiseFromInstancesCacheMap(cacheMap, key) {
      let item = cacheMap.get(key);
      if (item && item.then) {
        return item;
      } else {
          return new Promise((resolve, reject) => {
              resolve(item);
          })
      }
  }

  processItemRequest(cacheName, cacheMap, req, res) {
    const websiteInstance = this;

    const url = req.path;
    let html = websiteInstance.CACHED_DOCS.get(url) || websiteInstance.PAGES_CACHED_DOCS.get(url);
    if (html) {
        this.addHeadersToResult(res);
        res.send(html);
    } else {
        const date = new Date();
        console.log('cacheMap type', (typeof cacheMap).toString());
        console.log('cacheMap keys', Object.keys(cacheMap));
        websiteInstance.promiseFromInstancesCacheMap(cacheMap, req.params.slug).then(item => {
            console.log('pet read');
            websiteInstance.renderAndCache(url, [{ provide: cacheName, useValue: item, deps: [] },], websiteInstance.PAGES_CACHED_DOCS).then(html => {
                this.addHeadersToResult(res);
                console.log('loaded in', (((new Date().getTime()) - date.getTime()) / 1000));
                console.log('pet_', html);
                res.send(html);
            });
        }, error => {
            websiteInstance.renderAndCache(url, [], websiteInstance.PAGES_CACHED_DOCS).then(html => {
                this.addHeadersToResult(res);
                console.log('loaded in', (((new Date().getTime()) - date.getTime()) / 1000));
                console.log('pet__', html);
                res.send(html);
            });
        })
    }
  }

  getFromSlug(collection, slug) {
    const websiteInstance = this;

    return new Promise((resolve, reject) => {
        admin.firestore().collection(collection).where("slug", "==", slug).get().then(resultData => {
            if (resultData.empty) {
                return websiteInstance.getFromId(collection, getIdFromSlugified(slug)).then(resolve, reject);
            } else {
                let item = resultData.docs[0];
                let itemResult = item.data();
                itemResult.id = item.id;
                resolve(itemResult);
            }
        }, reject);
    });
  }

  getFromId(collection, id) {
    return new Promise((resolve, reject) => {
        admin.firestore().collection(collection).doc(id).get().then((doc) => {
            if (doc.exists) {
                let itemResult = doc.data();
                itemResult.id = doc.id;
                resolve(itemResult);
            } else {
                // TODO: Caso nao encontre deve tentar fazer busca por dominio
                resolve(null);
            }
        }, reject);
    });
  }
}