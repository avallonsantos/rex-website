import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

function setRating(ratedItem, ratedId, ratedUrl, newRatingValue) {
  var ratingSum = (ratedItem.ratingSum || 0) + newRatingValue;
  var ratingQtde = (ratedItem.ratingQtde || 0) + 1;
  var rating = ratingSum / ratingQtde;
  var updatedValues = {
    ratingSum: ratingSum,
    ratingQtde: ratingQtde,
    rating: rating
  }
  return admin.firestore().doc(ratedUrl).update(updatedValues);
}

function checkInterestRating(interest, previousInterest) {
  return new Promise((resolve, reject) => {
    if (previousInterest && previousInterest.hasOwnProperty('rating') && previousInterest.rating > -1) {
      // não precisa
      resolve(interest);
    } else {
      if (interest.hasOwnProperty('rating') && interest.rating > -1) {
        interest.pet.get().then(petSnap => {
          let petId = petSnap.id;
          let pet = petSnap.data();
  
          const promises = [];
          promises.push(pet.user.get());
          if (pet.place)
            promises.push(pet.place.get());
          Promise.all(promises).then(results => {
            let userOwnerId = results[0].id;
            let userOwner = results[0].data();
  
            let ratingPromises = [];
            ratingPromises.push(setRating(userOwner, userOwnerId, `users/${userOwnerId}`, interest.rating));
            if (results.length > 1) {
              let placeOwnerId = results[1].id;
              let placeOwner = results[1].data();
              ratingPromises.push(setRating(placeOwner, placeOwnerId, `places/${placeOwnerId}`, interest.rating));
            }
            Promise.all(ratingPromises).then(() => {
              resolve(interest);
            }, reject)
          }, reject);
        }, reject); 
      } else {
        resolve(interest);
      }
    }
  })
  
}

export const ratingsProvider = {
  checkInterestRating: checkInterestRating,
}