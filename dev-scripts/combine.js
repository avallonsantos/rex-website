let pet_fields = ["breed", "place_address.state", "place_type", "size", "type", "bornAt"];
let pet_collection = "pets";

function getAll(fields, collection) {
  var results = [];
  var Combinatorics = require('js-combinatorics');
  var combs = [];
  for (let index = 2; index <= 6; index++) {
    const cmb = Combinatorics.combination(fields, index);
    while(innerFields = cmb.next()) { 
      var item = { 
        collectionId: collection, 
        fields: (innerFields.map(function(field) {
          return { fieldPath: field, mode: "ASCENDING" };
        })),
      }
      results.push(item);
    }
  }
  return results;
}
