export const Environments = {
  production: {
    production: false,
    firebase: {
      apiKey: "AIzaSyBZu0iv8lPSRr9o7R63RL2k2NU3Hk7hTzI",
      authDomain: "rex-website-live.firebaseapp.com",
      databaseURL: "https://rex-website-live.firebaseio.com",
      projectId: "rex-website-live",
      storageBucket: "rex-website-live.appspot.com",
      messagingSenderId: "526365995073"
    },
    firebaseFunctions: {
      // baseUrl:"http://localhost:5001/rex-website-live/us-central1/",
      baseUrl: "https://us-central1-rex-website-live.cloudfunctions.net/",
      appPath: "app/"
    }
  },
  development: {
    production: false,
    firebase: {
      apiKey: "AIzaSyBZu0iv8lPSRr9o7R63RL2k2NU3Hk7hTzI",
      authDomain: "rex-website-live.firebaseapp.com",
      databaseURL: "https://rex-website-live.firebaseio.com",
      projectId: "rex-website-live",
      storageBucket: "rex-website-live.appspot.com",
      messagingSenderId: "526365995073"
    },
    firebaseFunctions: {
      // baseUrl:"http://localhost:5001/rex-website-live/us-central1/",
      baseUrl: "https://us-central1-rex-website-live.cloudfunctions.net/",
      appPath: "app/"
    }
  }
};

export const environment = Environments.development;
