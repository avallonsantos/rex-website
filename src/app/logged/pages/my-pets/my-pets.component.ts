import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { PetService } from '../../../shared/services/pet.service';
import { Pet } from '../../../global/models/pet';
import { User } from '../../../global/models/user';
import { SharedConfig } from '../../../shared/shared.config';
import { AuthService } from '../../../shared/services/auth.service';
import { AnalyticsService } from '../../../shared/services/analytics.service';

declare var PerfectScrollbar;

@Component({
  selector: 'logged-my-pets',
  templateUrl: './my-pets.component.html',
  styleUrls: ['./my-pets.component.scss']
})
export class MyPetsComponent implements OnInit, OnDestroy, AfterViewInit {

  results:Pet[] = [];
  groupedResults:Pet[][];

  selectedPet:any;
  showForm:boolean = false;

  currentUser: User;
  currentUserSub: any;

  userStat;
  userStatSub;

  constructor(private petService: PetService,
    private authService: AuthService,
    private analyticsService: AnalyticsService,
    private router: Router) { 
  }

  loadPets() {
    this.petService.loadMyPets(this.currentUser).then(list => {
      this.results = list;
      this.groupedResults = this.petService.groupResults(list, 3);

      this.userStatSub = this.analyticsService.currentUserStatSubjectObservable.subscribe(stat => {
        this.userStat = stat;
      })
    });
  }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      const currentUser = this.authService.currentUser;
      if (currentUser) {
        this.currentUser = currentUser;
        this.loadPets();
      } else {
        this.currentUserSub = this.authService.currentUserObservable.subscribe((user => {
          this.currentUser = user;
          if (user) {
            this.loadPets();
          } else {
            console.log("redir")
            this.router.navigate(["/"]);
          }
        }));
      }
    }
  }

  ngAfterViewInit() {
    if (!SharedConfig.instance().isServer) {
      const ps = new PerfectScrollbar('#add-ad');
    }
  }

  ngOnDestroy() {
    if (this.currentUserSub)
      this.currentUserSub.unsubscribe();

    if (this.userStatSub)
      this.userStatSub.unsubscribe();
  }

  addPet() {
    this.selectedPet = new Pet(null);
    this.showForm = true;
  }

  editPet(pet) {
    // this.selectedPet = new Pet();
    this.selectedPet = new Pet(pet.id, pet);
    this.showForm = true;
  }

  petFormClosed() {
    this.showForm = false;
    this.selectedPet = null;
  }

  petSaved(petSaved) {
    this.loadPets();

    this.showForm = false;
    this.selectedPet = null;
  }

  petRemoved() {
    this.loadPets();

    this.showForm = false;
    this.selectedPet = null;
  }

}
