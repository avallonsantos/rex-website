import { Component, OnInit } from '@angular/core';
import { Place } from '../../../global/models/place';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-place',
  template: '<shared-place-form [place]="place"></shared-place-form><app-footer></app-footer>',
  styleUrls: ['./edit-place.component.scss']
})
export class EditPlaceComponent implements OnInit {

  place: Place;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe((data: { place: Place }) => {
      if (data && data.place)
        this.place = data.place;
    });
  }

}
