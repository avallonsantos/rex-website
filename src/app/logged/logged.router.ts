import { Routes, RouterModule } from '@angular/router';
import {
    MyPetsComponent,
    EditPlaceComponent
} from './logged.pages';

import { PlaceOrCurrentResolver } from './resolvers/place-or-current.resolver';

const LOGGED_ROUTER: Routes = [
    {
        path: '',
        children: [
            {
                path: 'configurar-empresa',
                component: EditPlaceComponent,
                resolve: { place: PlaceOrCurrentResolver }
            },
            {
                path: 'configurar-empresa/:slug',
                component: EditPlaceComponent,
                resolve: { place: PlaceOrCurrentResolver }
            },
            {
                path: 'meus-rexs',
                component: MyPetsComponent,
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'meus-rexs'
            },
        ]
    },
    {
        path: '**',
        redirectTo: 'meus-rexs'
    }
];

export const LoggedRouter = RouterModule.forChild(LOGGED_ROUTER);

