import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FormsModule }   from '@angular/forms';

import { PetFormComponent } from './components/pet-form/pet-form.component';
import { MyPetsComponent } from './pages/my-pets/my-pets.component';
import { LoggedRouter } from './logged.router';
import { EditingModule } from '../editing/editing.module';
import { InterestCardComponent } from './components/interest-card/interest-card.component';
import { EditPlaceComponent } from './pages/edit-place/edit-place.component';
import { PlaceOrCurrentResolver } from './resolvers/place-or-current.resolver';

// import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
// import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
// const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = { };

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    LoggedRouter,
    FormsModule,
    EditingModule,
    // PerfectScrollbarModule
  ],
  declarations: [PetFormComponent, MyPetsComponent, InterestCardComponent, EditPlaceComponent],
  providers: [
    // { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
    PlaceOrCurrentResolver
  ],
})
export class LoggedModule { }
