import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injector, Injectable } from '@angular/core';
import { Place } from '../../global/models/place';
import { PlaceService } from '../../shared/services/place.service';
import { SharedConfig } from '../../shared/shared.config';
import { AuthService } from '../../shared/services/auth.service';
import { GlobalLayoutOptionsService } from '../../shared/services/global-layout-options.service';

@Injectable()
export class PlaceOrCurrentResolver implements Resolve<Place> {
  constructor(private placeService: PlaceService,
    private authService: AuthService,
    private globalLayoutOptionsService: GlobalLayoutOptionsService,
    private router: Router,
    private injector: Injector) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    this.globalLayoutOptionsService.setLoading(true);
    const injectedItem = SharedConfig.instance().isServer ? this.injector.get('place', null) : null;
    if (injectedItem) {
      this.globalLayoutOptionsService.setLoading(false);
      return new Place(injectedItem.id, injectedItem);
    } else {
      let slug = route.paramMap.get('slug');
      if (slug && slug.length > 0) {
        return this.placeService.getBySlug(slug).then(place => {
          this.globalLayoutOptionsService.setLoading(false);          
          if (place) {
            return place;
          } else { // id not found
            this.router.navigate(['/']);
            return null;
          }
        }, error => {
          console.log('error loading by Slug');
          this.globalLayoutOptionsService.setLoading(false);
          this.router.navigate(['/']);
        });
      } else {
        return new Promise((resolve, reject) => {
          this.authService.currentUserObservable.take(1).subscribe(user => {
            if (user) {
              this.placeService.listByUser(user).then(places => {
                this.globalLayoutOptionsService.setLoading(false);
                if (places.length > 0)
                  resolve(places[0]);
                else {
                  console.log('no place for currentUser');
                  this.globalLayoutOptionsService.setLoading(false);
                  this.router.navigate(['/']);
                  resolve(null);
                }
              }, error => {
                console.log('error loading places of user');
                this.globalLayoutOptionsService.setLoading(false);
                this.router.navigate(['/']);
                reject(error);
              });
            } else {
              console.log('no currentUser while searching places');
              this.globalLayoutOptionsService.setLoading(false);
              this.router.navigate(['/']);
              resolve(null);
            }
          })
        })
        
      }
    }
  }
}
