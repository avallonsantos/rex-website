import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { SharedConfig } from '../../../shared/shared.config';

import { PetService } from '../../../shared/services/pet.service';
import { AuthService } from '../../../shared/services/auth.service';
import { PlaceService } from '../../../shared/services/place.service';

import { Place } from '../../../global/models/place';
import { User } from '../../../global/models/user';
import { Pet, 
  YEAR_LIST, MONTH_LIST, 
  TYPE_LIST, OFFER_TYPE_LIST, 
  GENDER_LIST, SIZES_LIST
} from '../../../global/models/pet';

import { extendNoNulls } from '../../../global/utils/extends';
import { addDefaultToList, addDefaultToStringList } from '../../../global/utils/lists';

import { ImagePickerComponent } from '../../../shared/components/image-picker/image-picker.component';
import { GlobalLayoutOptionsService } from '../../../shared/services/global-layout-options.service';

@Component({
  selector: 'logged-pet-form',
  templateUrl: './pet-form.component.html',
  styleUrls: ['./pet-form.component.scss']
})
export class PetFormComponent implements OnInit, OnDestroy {

  private _pet:Pet;
  
  @Input("pet")
  set pet(pet: Pet) {
    this._pet = pet;
    this.galleryNewImages = [];
    this.onTypeChange();
  }
  get pet(): Pet { return this._pet; }

  @Output() formClosed = new EventEmitter;
  @Output() petSaved = new EventEmitter;

  @ViewChild('petForm') petForm;

  @ViewChild('selectedMainFileUpload') mainFileUpload: ImagePickerComponent;

  @ViewChild('galleryPicker') galleryPicker;
  galleryNewImages = [];
  galleryBasePath = "petGalleryImages";

  yearList = addDefaultToList(YEAR_LIST, "Anos");
  monthList = addDefaultToList(MONTH_LIST, "Meses");
  offerTypeList = addDefaultToList(OFFER_TYPE_LIST, "Situação");
  typeList = addDefaultToList(TYPE_LIST, "Tipo");
  genderList = addDefaultToList(GENDER_LIST, "Gênero");
  sizeList = addDefaultToList(SIZES_LIST, "Porte");
  
  placeholderBreed = "Raça";
  breedList = [ "Raça" ];
  
  submitted = false;

  currentUser: User;
  currentPlace: Place;
  currentUserSub;

  constructor(private petService: PetService,
    private placeService: PlaceService,
    private authService: AuthService,
    private globalLayoutOptionsService: GlobalLayoutOptionsService,
    private router: Router) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.currentUserSub = this.authService.currentUserObservable.subscribe(user => {
        this.currentUser = user;
        if (this.currentUser) {
          this.placeService.listByUser(user).then(places => {
            if (places && places.length > 0) {
              this.currentPlace = places[0];
            } else {
              this.currentPlace = null;
            }
          })
        } else {
          this.router.navigate(["/"]);
        }
      });
    }
  }

  ngOnDestroy() {
    this.unsubscribeCurrentUser();
  }

  unsubscribeCurrentUser() {
    if (this.currentUserSub)
      this.currentUserSub.unsubscribe();
  }

  onTypeChange() {
    if (this.pet && this.pet.type) {
      this.petService.breeds(this.pet.type).then(breeds => {
        this.breedList = addDefaultToStringList(breeds, this.placeholderBreed);
      });
    } else {
      this.breedList = [ this.placeholderBreed ]
    }
  }

  private handleError(error) {
    this.globalLayoutOptionsService.setLoading(false);
    console.log(error);
  }

  submitForm() {
    this.submitted = true;
    if (!this.petForm.form.valid)
      return;

    this._pet.user = this.currentUser;
    this._pet.place = this.currentPlace;

    const component = this;
    this.globalLayoutOptionsService.setLoading(true);
    this.mainFileUpload.uploadMainFile().then(mainImageFile => {
      if (mainImageFile) {
        this._pet.main_file_url = mainImageFile.url;
        this._pet.main_image = extendNoNulls({}, mainImageFile);
      }

      this.galleryPicker.processFiles().then(result => {
        for (const element of this.galleryNewImages) {
          if (element.uploaded)
            this._pet.gallery_images.push(element.uploaded);
        }

        this.petService.save(this._pet).then(() => {
          component.globalLayoutOptionsService.setLoading(false);
          this.petSaved.emit(this._pet);
        }, error => {
          component.handleError(error);
        });
      }, error => {
        component.handleError(error);
      })
    }, error => {
      component.handleError(error);
    });
  }

  closeForm() {
    this.formClosed.emit();
  }

}