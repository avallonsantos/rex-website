import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
// import { AuthGuard, RoleGuard } from './global/guards/guards';
import {
    HomeComponent, 
    FindFriendComponent, 
    ContactComponent,
    RegisterPlaceComponent,
    RegisterUserComponent,
    PetProfileComponent,
    PlaceProfileComponent,
    ManifestComponent,
    TermsComponent
  } from './global/pages/public';

import { PLACE_ONG, PLACE_CANIL } from './global/models/place';
import { PetResolver } from './shared/services/pet.service';
import { PlaceResolver } from './shared/services/place.service';
import { AdminGuard } from './shared/guards/admin.guard';

export const router: Routes = [
    {
        path: '',
        component: HomeComponent
    }, {
        path: 'home',
        component: HomeComponent
    }, {
        path: 'encontrar-melhor-amigo',
        component: FindFriendComponent
    }, {
        path: 'nosso-manifesto',
        component: ManifestComponent
    }, {
        path: 'contato',
        component: ContactComponent
    }, {
        path: 'termos',
        component: TermsComponent
    }, {
        path: 'registro-canil',
        component: RegisterPlaceComponent,
        data: { type: PLACE_CANIL.code  }
    }, {
        path: 'registro-ong',
        component: RegisterPlaceComponent,
        data: { type: PLACE_ONG.code }
    }, {
        path: 'registrar',
        component: RegisterUserComponent
    }, {
        path: 'pet/:slug',
        component: PetProfileComponent,
        resolve: { pet: PetResolver }
    }, {
        path: 'place/:slug',
        component: PlaceProfileComponent,
        resolve: { place: PlaceResolver }
    }, {
        path: 'conta',
        loadChildren: 'app/logged/logged.module#LoggedModule',
    }, {
        path: 'admin',
        loadChildren: 'app/admin/admin.module#AdminModule',
        canActivate: [AdminGuard],
    }, {
        path: '**',
        redirectTo: 'home'
    }
];

export const appRouter: ModuleWithProviders = RouterModule.forRoot(
    router,
    // { enableTracing: true }
);
