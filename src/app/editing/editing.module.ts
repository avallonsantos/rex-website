import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadService } from './services/upload.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [UploadService]
})
export class EditingModule { }
