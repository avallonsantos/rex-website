export class UploadFile extends File {
  public progress: Number;
  public url: String = '';
  public relativeUrl: String = '';
}
