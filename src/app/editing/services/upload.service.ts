import { Injectable } from '@angular/core';

import { UploadFile } from '../models/upload-file';

import * as firebase from 'firebase';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';


@Injectable()
export class UploadService {

  constructor(private storage: AngularFireStorage) {

  }

  uploadFileDataUrl(fileDataUrl: string, filename: string, basePath: string): Promise<any> {
    const storageRef = firebase.storage().ref();

    const promise = new Promise((resolve, reject) => {
      try {
        storageRef.child(`${basePath}/${filename}.jpg`)
          .putString(fileDataUrl, firebase.storage.StringFormat.DATA_URL).then(snapshot => {
            resolve(snapshot.downloadURL);
          }, reject);
      } catch (ex) {
        reject(ex);
      }
    });
    return promise;
  }

  uploadFile(fileToUpload: UploadFile, basePath: string): Promise<any> {
    const promise = new Promise((resolve, reject) => {
      try {
        const uploadPath = `${basePath}/${fileToUpload.name}`;
        const uploadTask: AngularFireUploadTask = this.storage.ref(uploadPath).put(fileToUpload);
        uploadTask.then(result => {
          fileToUpload.url = result.downloadURL;
          fileToUpload.relativeUrl = uploadPath;
          resolve(fileToUpload);
        }, error => {
          console.error(error);
          reject(error);
        });
      } catch (ex) {
        reject(ex);
      }
    });
    return promise;
  }
}
