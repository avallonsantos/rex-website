import { NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';

import * as firebase from '@firebase/app';

import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';

import { AppModule } from './app.module';

@NgModule({
  bootstrap: [ AppComponent ],
  imports: [
    AppModule,
    AngularFireModule.initializeApp(environment.firebase),
  ]
})
export class AppBrowserModule {}