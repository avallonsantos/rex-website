export function extendObj<A>(a: A): A;
export function extendObj<A, B>(a: A, b: B): A & B;
export function extendObj<A, B, C>(a: A, b: B, c: C): A & B & C;
export function extendObj<A, B, C, D>(a: A, b: B, c: C, d: D): A & B & C & D;
export function extendObj(...args: any[]): any {
    const newObj = args[0];
    for (let i = 1; i < args.length; i++) {
        const obj = args[i];
        for (const key in obj) {
          if (obj.hasOwnProperty(key)) {
            newObj[key] = obj[key];
          }
        }
    }
    return newObj;
};

export function extendNoNulls<A>(a: A): A;
export function extendNoNulls<A, B>(a: A, b: B): A & B;
export function extendNoNulls<A, B, C>(a: A, b: B, c: C): A & B & C;
export function extendNoNulls<A, B, C, D>(a: A, b: B, c: C, d: D): A & B & C & D;
export function extendNoNulls(...args: any[]): any {
    const newObj = args[0];
    for (let i = 1; i < args.length; i++) {
        const obj = args[i];
        for (const key in obj) {
          if (obj.hasOwnProperty(key)) {
            if (!(obj[key] === undefined || obj[key] == null)) {
              newObj[key] = obj[key];
            }
          }
        }
    }
    return newObj;
};
