export const WEEKDAYS = [
  {"code":0,"name":"Domingo"},
  {"code":1,"name":"Segunda"},
  {"code":2,"name":"Terça"},
  {"code":3,"name":"Quarta"},
  {"code":4,"name":"Quinta"},
  {"code":5,"name":"Sexta"},
  {"code":6,"name":"Sábado"},
]