export const addDefaultToList = (list, name, code = null) => {
  let newList = list.slice(0);
  newList.splice(0, 0, { name, code });
  return newList;
}

export const addDefaultToStringList = (list, name) => {
  let newList = list.slice(0);
  newList.splice(0, 0, name);
  return newList;
}

export const findFirst = (list, condition) => {
  for (let index = 0; index < list.length; index++) {
    const item = list[index];
    if (condition(item)) {
      return item;
    }
  }
  return null;
}

export function groupList<T>(results: T[], groupSize:number) {
  let currentGroup = [];
  const groups = [currentGroup];
  results.forEach(element => {
    if (currentGroup.length < groupSize) {
      currentGroup.push(element);
    } else {
      currentGroup = [ element ];
      groups.push(currentGroup);
    }
  });
  if (groups.length == 1 && groups[0].length == 0)
    return [];
  return groups;
};