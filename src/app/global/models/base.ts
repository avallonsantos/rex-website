import { extendObj } from '../utils/extends';
import { slugifyWithId } from '../utils/string';

export class Base {
  public id: string;
  public createdAt: Date;

  constructor(id?:string, dbData?: any) {
    this.id = id;
    this.createdAt = new Date();
    if (dbData) {
      extendObj(this, dbData);
    }
  }

  calculateSlug(name) {
    if (this.id) {
      return slugifyWithId(name, this.id);
    } else {
      return null;
    }
  }
}