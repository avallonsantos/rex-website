export interface IPetStats {
  meeting: Number;
  friend: Number;
  visit: Number;
}

export interface IPetListStats {
  [petKey: string]: IPetStats;
}

export interface IUserStats {
  pets: IPetListStats
}

export interface IPlaceStats {
  pets: IPetListStats
}