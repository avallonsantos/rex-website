import { Base } from './base';
import { extendNoNulls } from '../utils/extends';

export interface IPetProfile {
  calm: number;
  easy: number;
  caring: number;
  social: number;
  friendly: number;
}

export class Pet extends Base {
  name:string;
  slug:string;
  offer_type?:string;
  gender?:string;
  size:string;
  type:string;
  breed:string;
  
  bornAt:Date;
  age:number;
  age_month:number;

  main_file_url:string;
  main_image: any;
  gallery_images:any[];

  address:string;
  description:string;
  reason:string;
  estimated_age: boolean;
  pedigree:string;
  
  vacinas:any;
  profile: IPetProfile;
  user: any;

  place: any;
  place_type: string;
  place_address: string;

  owner_name: string;
  owner_image_url: string;

  constructor(id, dbData?: any) {
    super(id, dbData);
    if (!this.vacinas) this.vacinas = {}
    if (!this.user) this.user = {}
    if (!this.place) this.place = {}
    if (!this.profile) this.profile = { calm: 50, easy: 50, caring: 50, social: 50, friendly: 50, }
    if (!this.main_image) this.main_image = {};
    if (!this.gallery_images) this.gallery_images = [];
    if (!this.slug) this.slug = this.calculateSlug(this.name);
    
    if (this.bornAt) { 
      this.calculateAge();
    } else {
      this.calculateBornAt();
    }

    if (!dbData) {
      this.age = null;
      this.age_month = null;
      this.offer_type = null;
      this.type = null;
      this.gender = null;
      this.breed = null;
      this.size = null;
    }
  }

  getOrCalculateSlug() {
    return this.slug || this.calculateSlug(this.name) || this.id;
  }
  calculateBornAt() {
    if (this.age && this.age_month) {
      const age_year = parseInt(this.age.toString());
      const age_month = parseInt(this.age_month.toString());
      
      if (age_year >= 0 && age_month >= 0) {
        const bornAt = new Date();
        bornAt.setDate(1);
        bornAt.setHours(0, 0, 0, 0);
        if (age_month > 0)
          bornAt.setMonth(bornAt.getMonth() - age_month);
        if (age_year > 0)
          bornAt.setFullYear(bornAt.getFullYear()-age_year);
        this.bornAt = bornAt;
      }
    }
  }
  calculateAge() {
    const now = new Date();
    now.setDate(1);
    now.setHours(0, 0, 0, 0);

    var months = (now.getFullYear() - this.bornAt.getFullYear()) * 12;
    months += now.getMonth() - this.bornAt.getMonth();

    this.age = Math.floor(months / 12);
    this.age_month = Math.floor(months - (this.age * 12));
  }

  toFirestore(firestore) {
    const item = this;

    if (item.age && item.age_month)
      item.calculateBornAt();
    if (!item.slug)
      item.slug = item.calculateSlug(item.name);

    const firestoreData: any = {};
    extendNoNulls(firestoreData, item);
    if (firestoreData.user) {
      if (typeof firestoreData.user === 'string') {
        firestoreData.user = firestore.doc(`users/${firestoreData.user}`).ref;
      } else { 
        if (firestoreData.user.id)
          firestoreData.user = firestore.doc(`users/${firestoreData.user.id}`).ref;
        if (firestoreData.user.displayName)
          firestoreData.owner_name = firestoreData.user.displayName;
        if (item.user.main_file_url)
          firestoreData.owner_image_url = item.user.main_file_url;
      }
    }
    if (firestoreData.place) {
      if (typeof firestoreData.place === 'string') {
        firestoreData.place = firestore.doc(`places/${firestoreData.place}`).ref;
      } else {
        if (item.place.id)
          firestoreData.place = firestore.doc(`places/${firestoreData.place.id}`).ref;
        if (item.place.type)
          firestoreData.place_type = item.place.type;
        if (item.place.address)
          firestoreData.place_address = item.place.address;
        if (item.place.name)
          firestoreData.owner_name = item.place.name;
        if (item.place.main_file_url)
          firestoreData.owner_image_url = item.place.main_file_url;
      }
    }
    if (item.main_image)
      item.main_image = extendNoNulls({}, item.main_image);
    for (let index = 0; index < item.gallery_images.length; index++) {
      item.gallery_images[index] = extendNoNulls({}, item.gallery_images[index]);
    }

    delete firestoreData['_gender'];
    delete firestoreData['createdAt'];
    return firestoreData;
  }

  genderObj() {
    if (this['_gender']) {
      return this['_gender'];
    } else if (this.gender) {
      if (this.gender == GENDER_MALE.code) {
        this['_gender'] = GENDER_MALE;
        return GENDER_MALE;
      } else if (this.gender == GENDER_FEMALE.code) {
        this['_gender'] = GENDER_FEMALE;
        return GENDER_FEMALE;
      } else {
        return null;
      }
    } else {
      delete this['_gender'];
      return { name: "-" };
    }
  }
}

export const YEAR_RANGE_LIST: any[] = [
  { "name": "0 - 1 ano", "code": "0-1" },
  { "name": "1 - 4 anos", "code": "1-4" },
  { "name": "4 - 6 anos", "code": "4-6" },
  { "name": "Acima de 6 anos", "code": "6-100" },
]

export const YEAR_LIST : any[] =[
  { "name": "0 anos", "code": 0 },
  { "name": "1 ano", "code": 1 },
  { "name": "2 anos", "code": 2 },
  { "name": "3 anos", "code": 3 },
  { "name": "4 anos", "code": 4 },
  { "name": "5 anos", "code": 5 },
  { "name": "6 anos", "code": 6 },
  { "name": "7 anos", "code": 7 },
  { "name": "8 anos", "code": 8 },
  { "name": "9 anos", "code": 9 },
  { "name": "10 anos", "code": 10 },
  { "name": "11 anos", "code": 11 },
  { "name": "12 anos", "code": 12 },
  { "name": "13 anos", "code": 13 },
  { "name": "14 anos", "code": 14 },
  { "name": "15 ou mais", "code": 15 },
];

export const MONTH_LIST : any[] =[
  { "name": "0 meses", "code": 0 },
  { "name": "1 mês", "code": 1 },
  { "name": "2 meses", "code": 2 },
  { "name": "3 meses", "code": 3 },
  { "name": "4 meses", "code": 4 },
  { "name": "5 meses", "code": 5 },
  { "name": "6 meses", "code": 6 },
  { "name": "7 meses", "code": 7 },
  { "name": "8 meses", "code": 8 },
  { "name": "9 meses", "code": 9 },
  { "name": "10 meses", "code": 10 },
  { "name": "11 meses", "code": 11 }
];

export const TYPE_LIST : any[] =[
  { "name": "Cachorro", "code": "dog" },
  { "name": "Gato", "code": "cat" },
];

export const OFFER_TYPE_LIST : any[] =[
  { "name": "Doação", "code": "donation" },
  { "name": "Venda", "code": "sale" },
];

export const SIZES_LIST : any[] =[
  { "name": "Grande", "code": "big" },
  { "name": "Médio", "code": "medium" },
  { "name": "Pequeno", "code": "small" },
];

export const GENDER_MALE = { "name": "Macho", "code": "male", icon: 'public/img/_icons/male.svg' };
export const GENDER_FEMALE = { "name": "Fêmea", "code": "female", icon: 'public/img/_icons/female.svg' };
export const GENDER_LIST : any[] = [ GENDER_MALE, GENDER_FEMALE,];