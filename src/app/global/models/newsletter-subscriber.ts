import { Base } from './base';
import { extendNoNulls } from '../utils/extends';

export class NewsletterSubscriber extends Base {
  email:string;

  constructor(id, dbData?: any) {
    super(id, dbData);
    
    if (!dbData) {
      this.email = null;
    }
  }

  toFirestore(firestore) {
    const item = this;

    const firestoreData: any = {};
    extendNoNulls(firestoreData, item);
    
    delete firestoreData['_gender'];
    delete firestoreData['createdAt'];
    return firestoreData;
  }
}