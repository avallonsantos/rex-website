export interface IAddress {
  phone: string;
  street: string;
  number: string;
  complement: string;
  postal_code: string;
  city: string;
  state: string;
}