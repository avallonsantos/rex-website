import { Base } from './base';

import { extendNoNulls } from '../utils/extends';
import { IAddress } from './address';

export interface IUserInteresses {
  racas_procuradas: boolean;
  racas_exoticas: boolean;
  sem_raca_definida: boolean;
  encontrar_lar: boolean;
  comprar_produtos: boolean;
  ficar_por_dentro: boolean;
  conseguir_descontos: boolean;
  informacoes: boolean;
}

export interface IUserProfile {
  home_type: number;
  home_location: number;
  work_away: number;
  weekend_away: number;
  travel_away: number;
}

export class User extends Base {
  displayName:string;
  email:string;
  gender:string;
  cpf:string;

  main_file_url:string;
  main_image: any;
  gallery_images:any[];

  interesses: IUserInteresses;
  breeds;
  profile: IUserProfile;
  address: IAddress;

  ownedPlace;

  rating:number;

  constructor(id?:string, dbData?: any) {
    super(id, dbData);
    if (!this.interesses) {
      this.interesses = {
        racas_procuradas: false,
        racas_exoticas: false,
        sem_raca_definida: false,
        encontrar_lar: false,
        comprar_produtos: false,
        ficar_por_dentro: false,
        conseguir_descontos: false,
        informacoes: false
      }
    }
    if (!this.profile) { 
      this.profile = {
        home_type: 50,
        home_location: 50,
        work_away: 50,
        weekend_away: 50,
        travel_away: 50,
      }
    }
    if (!this.breeds) this.breeds = [];

    if (!this.main_image) this.main_image = {};
    if (!this.gallery_images) this.gallery_images = [];
    if (!this.ownedPlace) this.ownedPlace = {};
    if (!this.rating) this.rating = null;
    if (!this.address) this.address = { 
      phone: null,
      street: null,
      number: null,
      complement: null,
      postal_code: null,
      city: null,
      state: null,
    }

    if (!dbData) {
      this.displayName = null;
      this.email = null;
      this.gender = null;
      this.cpf = null;
    }
  }

  isAdminUser() {
   return this["isAdmin"] ? true : false;
  }

  applyAuthInfo(authInfo) {
    if (!this.id)
      this.id = authInfo.uid;

    if (!this.email)
      this.email = authInfo.email;

    if (!this.displayName)
      this.displayName = authInfo.displayName;

    if (!this.main_file_url)
      this.main_file_url = this.getAuthPhotoURL(authInfo);
  }

  private getAuthPhotoURL(authInfo) {
    for (let i = 0; i < authInfo.providerData.length; i++) {
        let providerInfo = authInfo.providerData[i];
        if (providerInfo.photoURL) {
          return providerInfo.photoURL;
        }
    }
    return null;
  }

  toFirestore(firestore) {
    const firestoreData: any = {};
    extendNoNulls(firestoreData, this);
    if (this.main_image) {
      this.main_image = extendNoNulls({}, this.main_image);
    }
    for (let index = 0; index < this.gallery_images.length; index++) {
      firestoreData.gallery_images[index] = extendNoNulls({}, this.gallery_images[index]);
    }
    if (!this.breeds || this.breeds.length == 0)
      delete firestoreData['breeds'];
    if (!this.main_image || Object.keys(this.main_image).length == 0)
      delete firestoreData['main_image'];
    
    if (firestoreData.ownedPlace ) {
      if (typeof firestoreData.ownedPlace === 'string') {
        firestoreData.ownedPlace = firestore.doc(`places/${firestoreData.ownedPlace}`).ref;
      } else if (firestoreData.ownedPlace.id) {
        firestoreData.ownedPlace = firestore.doc(`places/${firestoreData.ownedPlace.id}`).ref;
      }
    }

    delete firestoreData['createdAt'];
    return firestoreData;
  }
}

export const USER_GENDER_FEMALE = { name: "Fêmea", code: "female" };
export const USER_GENDER_MALE = { name: "Macho", code: "male" };
export const USER_GENDER_LIST : any[] =[
  USER_GENDER_FEMALE,
  USER_GENDER_MALE,
];