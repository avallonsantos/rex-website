import { Base } from './base';
import { IAddress } from './address';
import { extendNoNulls } from '../utils/extends';

export class Interest extends Base {
  rating: number;
  type: string;

  user: any;
  pet: any;

  constructor(id?:string, dbData?: any) {
    super(id, dbData);

    if (!this.user) this.user = {};
    if (!this.pet) this.pet = {};
    
    if (!dbData) {
      this.type = null;
      this.rating = null;
    }
  }

  toFirestore(firestore) {

    const firestoreData: any = {};
    extendNoNulls(firestoreData, this);
    if (firestoreData.user) {
      if (typeof firestoreData.user === 'string') {
        firestoreData.user = firestore.doc(`users/${firestoreData.user}`).ref;
      } else if (firestoreData.user.id) {
        firestoreData.user = firestore.doc(`users/${firestoreData.user.id}`).ref;
      }
    }
    if (firestoreData.pet) {
      if (typeof firestoreData.pet === 'string') {
        firestoreData.pet = firestore.doc(`pets/${firestoreData.pet}`).ref;
      } else if (firestoreData.pet.id) {
        firestoreData.pet = firestore.doc(`pets/${firestoreData.pet.id}`).ref;
      }
    }

    delete firestoreData['createdAt'];
    return firestoreData;
  }
}