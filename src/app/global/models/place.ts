import { Base } from './base';
import { IAddress } from './address';
import { extendNoNulls } from '../utils/extends';

export class Place extends Base {
  name:string;
  cnpj:string;
  type:string;
  slug:string;

  presentation:string;
  proposal:string;
  tags;

  workDaysStart: number;
  workDaysEnd: number;
  workHoursStart: number;
  workHoursEnd: number;

  main_file_url:string;
  main_image: any;
  cover_file_url:string;
  cover_image: any;
  gallery_images:any[];

  user: any;

  breeds;
  address: IAddress;

  rating:number;

  constructor(id?:string, dbData?: any) {
    super(id, dbData);
    if (!this.slug) this.slug = this.calculateSlug(this.name);
    if (!this.user) this.user = {}
    if (!this.breeds) this.breeds = [];
    if (!this.tags) this.tags = [];
    if (!this.address) this.address = { 
      phone: null,
      street: null,
      number: null,
      complement: null,
      postal_code: null,
      city: null,
      state: null,
    }

    if (!this.main_image) this.main_image = {};
    if (!this.cover_image) this.cover_image = {};
    if (!this.gallery_images) this.gallery_images = [];
    if (!this.rating) this.rating = null;

    if (!dbData) {
      this.name = null;
      this.cnpj = null;
      this.type = null;

      this.workDaysStart = null;
      this.workDaysEnd = null;

      this.workHoursStart = null;
      this.workHoursEnd = null;
    }
  }

  toFirestore(firestore) {
    if (!this.slug)
      this.slug = this.calculateSlug(this.name);

    const firestoreData: any = {};
    extendNoNulls(firestoreData, this);
    if (firestoreData.user) {
      if (typeof firestoreData.user === 'string') {
        firestoreData.user = firestore.doc(`users/${firestoreData.user}`).ref;
      } else if (firestoreData.user.id) {
        firestoreData.user = firestore.doc(`users/${firestoreData.user.id}`).ref;
      }
    }
    this.main_image = extendNoNulls({}, this.main_image);
    this.cover_image = extendNoNulls({}, this.cover_image);
    for (let index = 0; index < this.gallery_images.length; index++) {
      this.gallery_images[index] = extendNoNulls({}, this.gallery_images[index]);
    }
    if (!this.breeds || this.breeds.length == 0)
      delete firestoreData['breeds'];
    if (!this.tags || this.tags.length == 0)
      delete firestoreData['tags'];
    if (!this.main_image || Object.keys(this.main_image).length == 0)
      delete firestoreData['main_image'];
    if (!this.cover_image || Object.keys(this.cover_image).length == 0)
      delete firestoreData['cover_image'];

    delete firestoreData['createdAt'];
    return firestoreData;
  }

  getOrCalculateSlug() {
    return this.slug || this.calculateSlug(this.name) || this.id;
  }
}

export const PLACE_CANIL = { "name": "Canil", "code": "canil" };
export const PLACE_ONG = { "name": "ONG", "code": "ong" };
export const PLACE_TYPE_LIST : any[] = [ PLACE_CANIL, PLACE_ONG ];