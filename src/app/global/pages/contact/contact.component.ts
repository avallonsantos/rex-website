import { Component, OnInit, ViewChild } from '@angular/core';
import { GlobalLayoutOptionsService } from '../../../shared/services/global-layout-options.service';
import { ContactService, ContactData } from '../../../shared/services/contact.service';
import { AuthService } from '../../../shared/services/auth.service';
import { SharedConfig } from '../../../shared/shared.config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  @ViewChild('contactForm') contactForm;
  contactData: ContactData = { email: "", displayName: "", subject: "", message: "" };

  submitted = false;
  sent = false;

  constructor(private contactService: ContactService,
    private authService: AuthService,
    private router: Router,
    private globalLayoutOptionsService: GlobalLayoutOptionsService) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.authService.currentUserObservable.take(1).subscribe(user => {
        if (user) {
          this.contactData.email = user.email;
          this.contactData.displayName = user.displayName;
        }
      })
    }
  }

  validate() {
    return this.contactForm.form.valid;
  }

  private handleError(error) {
    this.globalLayoutOptionsService.setLoading(false);
    this.sent = false;
  }

  send() {
    if (!this.validate()) {
      return;
    }
    this.globalLayoutOptionsService.setLoading(true);
    this.submitted = true;
    this.sent = false;

    this.contactService.sendContact(this.contactData).then(result => {
      this.globalLayoutOptionsService.setLoading(false);
      this.sent = true;
    }, this.handleError);
  }

  redirectToRoot() {
    this.router.navigate(["/"]);
  }

}
