import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SharedConfig } from '../../../shared/shared.config';
import { Place } from '../../models/place';

@Component({
  selector: 'app-register-place',
  styleUrls: ['./register-place.component.css'],
  template: '<shared-place-form [place]="place"></shared-place-form><app-footer></app-footer>'
})
export class RegisterPlaceComponent implements OnInit {

  place: Place;

  constructor(private route: ActivatedRoute) {  }

  ngOnInit() {
    this.route.data.subscribe((data: { type: string }) => {
      this.place = new Place();
      this.place.type = data.type;
    });
    
  }
}
