import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { User, USER_GENDER_FEMALE, USER_GENDER_MALE } from '../../models/user';
import { Router } from '@angular/router';

import { AuthService } from '../../../shared/services/auth.service';
import { UploadService } from '../../../editing/services/upload.service';
import { SharedConfig } from '../../../shared/shared.config';
import { GlobalLayoutOptionsService } from '../../../shared/services/global-layout-options.service';
import { ImagePickerComponent } from '../../../shared/components/image-picker/image-picker.component';
import { extendNoNulls } from '../../utils/extends';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit, OnDestroy {

  cpfMask = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  phoneMask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
  cnpjMask = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];

  user:User;
  insertedBreed: "";
  password: "";
  password_confirmation: "";
  gender_male?: boolean;

  @ViewChild('userForm') userForm;
  
  @ViewChild('selectedMainFileUpload') mainFileUpload: ImagePickerComponent;
  mainImageBasePath = "userMainImages";

  @ViewChild('galleryPicker') galleryPicker;
  galleryNewImages = [];
  galleryBasePath = "userGalleryImages";
  
  submitted: boolean = false;

  currentUserSub;

  constructor(private authService: AuthService,
    private uploadService: UploadService,
    private router: Router,
    private globalLayoutOptionsService: GlobalLayoutOptionsService) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      const currentUser = this.authService.currentUser;
      if (!currentUser) {
        this.currentUserSub = this.authService.currentUserObservable.subscribe((user => {
          this.checkUser(user);
          if (user)
            this.unsubscribeCurrentUser();
        }));
      }

      this.readCurrentUserInfo(currentUser);
    }
  }

  readCurrentUserInfo(currentUser: User) {
    if (currentUser) {
      this.user = currentUser;
      this.unsubscribeCurrentUser();
    } else {
      if (!this.user) {
        this.user = new User();
        this.user.gender = USER_GENDER_FEMALE.code;
      }
      if (this.authService.currentAuthUser)
        this.user.applyAuthInfo(this.authService.currentAuthUser);
    }

    this.gender_male = this.user.gender != USER_GENDER_FEMALE.code;
  }

  checkUser(currentUser) {
    if (!this.authService.requiresRegistration()){
      this.redirectToPrevious();
      return;
    }

    this.readCurrentUserInfo(currentUser);
  }

  unsubscribeCurrentUser() {
    if (this.currentUserSub)
      this.currentUserSub.unsubscribe();
  }

  ngOnDestroy() {
    this.unsubscribeCurrentUser();
  }

  redirectToPrevious() {
    this.globalLayoutOptionsService.setLoading(false);
    this.router.navigate(this.authService.loginNavParams ? this.authService.loginNavParams : ["/"]);
    this.authService.clearLoginNavParams();
  }

  breedKeyDown(event) {
    if (event.keyCode == 13 || event.keyCode == 188) {
      this.insertBreed();
      if (event.preventDefault) {
        event.preventDefault();
      }
    }
  }

  insertBreed() {
    let value = this.insertedBreed.trim();
    if (value.length > 0) {
      this.user.breeds.push(value);
      this.insertedBreed = "";
    }
  }

  removeBreed(breed) {
    const index = this.user.breeds.indexOf(breed);
    if (index > -1) {
      this.user.breeds.splice(index, 1);
    }
  }

  genderChanged() {
    this.user.gender = this.gender_male ? USER_GENDER_MALE.code : USER_GENDER_FEMALE.code;
  }

  validate() {
    return this.userForm.form.valid;
  }

  private doRegister() {
    return new Promise((resolve, reject) => {
      this.authService.registerManual(this.user, this.password).then(resolve, reject);
    });
  }

  private doUploadImages() {
    const component = this;
    return new Promise((resolve, reject) => {
      let userKey = this.user.id ? (this.user.id + "-") : "";
      this.mainImageBasePath = `userMainImages/${userKey}`;
      this.mainFileUpload.uploadMainFile().then(mainImageFile => {
        if (mainImageFile) {
          this.user.main_file_url = mainImageFile.url;
          this.user.main_image = extendNoNulls({}, mainImageFile);;
        }
  
        this.galleryBasePath = `userGalleryImages/${userKey}`;
        this.galleryPicker.processFiles().then(result => {
          for (const element of this.galleryNewImages) {
            if (element.uploaded)
              this.user.gallery_images.push(element.uploaded);
          }
  
          resolve(this.user);
        }, error => {
          component.handleError(error);
        })
      }, error => {
        component.handleError(error);
      });
    });
  }

  private handleError(error) {
    this.globalLayoutOptionsService.setLoading(false);
  }


  save() {
    if (!this.validate()) {
      return;
    }
    
    this.unsubscribeCurrentUser();
    this.globalLayoutOptionsService.setLoading(true);

    this.submitted = true;
    this.doRegister().then(result => {
      this.doUploadImages().then(result => {
        this.authService.updateUserInfo(this.user).then(result => {
          this.redirectToPrevious();
        }, this.handleError)
      }, this.handleError);
    }, this.handleError);
  }

}
