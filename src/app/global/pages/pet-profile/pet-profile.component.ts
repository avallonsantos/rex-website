import { Component, OnInit, OnDestroy } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { PetService } from '../../../shared/services/pet.service';
import { PlaceService } from '../../../shared/services/place.service';
import { SharedConfig } from '../../../shared/shared.config';
import { Pet } from '../../models/pet';
import { AnalyticsService } from '../../../shared/services/analytics.service';
import { AuthService } from '../../../shared/services/auth.service';
import { GlobalLayoutOptionsService } from '../../../shared/services/global-layout-options.service';

declare const $;

@Component({
  selector: 'app-pet-profile',
  templateUrl: './pet-profile.component.html',
  styleUrls: ['./pet-profile.component.scss']
})
export class PetProfileComponent implements OnInit, OnDestroy {

  pet;
  place;
  breed = null;

  currentUser;
  viewMarked: boolean = false;

  userStat;
  userStatSub;
  petStat;
  interestSaved:boolean = false;

  constructor(private route: ActivatedRoute, private router: Router,
    private petService: PetService, private placeService: PlaceService,
    private analyticsService: AnalyticsService, private authService: AuthService,
    private globalLayoutOptionsService: GlobalLayoutOptionsService) { }

  ngOnInit() {
    this.interestSaved = false;
    this.viewMarked = false;

    let pet = this.route.data["pet"];
    if (pet) {
      this.setPet(pet);
    } else {
      this.route.data.subscribe((data: { pet: Pet }) => {
        if (data && !pet) {
          this.setPet(data.pet);
        }
      });
    }

    this.globalLayoutOptionsService.setMobileSearchVisibility(false);

    if (!SharedConfig.instance().isServer) {
      var limit = ($('footer').offset().top) - 100;
    
      $('.navegation-active, .can-be-fixed').scrollToFixed({
        top: 100,
        limit: limit
      });
    
      $(document).on("scroll", this.onScroll);
    }
  }

  ngOnDestroy() {
    if (this.userStatSub)
      this.userStatSub.unsubscribe();
  }

  setPet(pet: Pet) {
    this.pet = pet;
    if (this.pet) {
      this.onPetLoaded();
      
      if (!SharedConfig.instance().isServer) {
        this.authService.currentUserObservable.subscribe(user => {
          if (this.currentUser != user) {
            this.currentUser = user;
            if (!SharedConfig.instance().isServer && !this.viewMarked) {
              if (!this.currentUser || this.currentUser.id != this.pet.user.id) {
                // verifica se deve marcar estatistica senao marca visita
                if (!this.currentUser || !this.checkStatFromParam()) {
                  console.log('visiting');
                  this.viewMarked = true;
                  this.analyticsService.visitPet(this.pet);
                }
              }
            }
          }
        });
        
        if (this.pet.place) {
          this.placeService.retrieveFromObject(this.pet.place).then(place => {
            this.place = place;
          })
        }
  
        this.userStatSub = this.analyticsService.currentUserStatSubjectObservable.subscribe(stat => {
          this.userStat = stat;
          if (this.userStat && this.userStat.pets && this.userStat.pets[this.pet.id]) {
            this.petStat = this.userStat.pets[this.pet.id];
          } else if (this.currentUser && this.currentUser.isAdminUser()) {
            this.analyticsService.petStat(this.pet).then(petStat => {
              this.petStat = petStat || {};
            });
          }
        }) 
      }
    }
  }

  views() {
    return this.petStat ? (this.petStat.visit || 0) : 0;
  }

  checkStatFromParam() {
    const stat_type = this.route.snapshot.paramMap.get('stat_type');
    if (stat_type) {
      console.log('stat_type', stat_type);
      this.requestStat(stat_type);
      return true;
    } else {
      return false;
    }
  }

  onPetLoaded() {
    if (!SharedConfig.instance().isServer) {
      setTimeout(() => {
        $('.slide-pet ul').slick({
          autoplay: true,
          autoplaySpeed: 5000,
          prevArrow: '<img class="prev-arrow" src="public/img/_icons/arrow-slide-left.svg">',
          nextArrow: '<img class="next-arrow" src="public/img/_icons/arrow-slide-right.svg">',
        });
      }, 50);
    }

    $('.swipebox').swipebox();
  }

  private onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('.navegation-active li .page-local').each(function () {
      var currLink = $(this);
      var refElement = $(currLink.attr("data-target"));
      if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
          $('.navegation-active li .page-local').removeClass("active");
          currLink.addClass("active");
      }
      else {
          currLink.removeClass("active");
      }
    });
  }

  requestStat(type) {
    this.interestSaved = false;
    if (this.currentUser) {
      this.analyticsService.saveInterest(this.pet, this.currentUser, type).then(result => {
        console.log('interst saved');
        this.interestSaved = true;
      }, error => {
        console.error('interst error', error);
      });
    } else {
      this.authService.requestLogin([ "/pet", { slug: this.pet.slug, stat_type: type } ]);
    }
  }

  onSearchSubmit(search) {
    this.router.navigate(["/encontrar-melhor-amigo"], { queryParams: { from: 'pet' } });
  }

}
