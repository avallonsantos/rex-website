import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { PetService, SearchData } from '../../../shared/services/pet.service';

import { Pet } from '../../../global/models/pet';
import { SharedConfig } from '../../../shared/shared.config';
import { SearchStateService } from '../../../shared/services/search-state.service';
import { GlobalLayoutOptionsService } from '../../../shared/services/global-layout-options.service';
import { AuthService } from '../../../shared/services/auth.service';
import { ActivatedRoute } from '@angular/router';

declare var $;

@Component({
  selector: 'app-find-friend',
  templateUrl: './find-friend.component.html',
  styleUrls: ['./find-friend.component.css']
})
export class FindFriendComponent implements OnInit {
  
  results:any;
  groupedResults:any;

  showCardStats: boolean = false;

  lastSearch;
  searchSub;
  authSub;

  shouldScrollToContent = false;
  shouldHideOnMobile = false;

  constructor(private petService: PetService,
    private searchStateService: SearchStateService,
    private authService: AuthService,
    private globalLayoutOptionsService: GlobalLayoutOptionsService,
    private route: ActivatedRoute,
    private injector: Injector) { 
      if (!SharedConfig.instance().isServer) {
        this.searchSub = this.searchStateService.searchObservable.subscribe(searchData => {
          if (this.lastSearch != searchData) {
            this.lastSearch = searchData;
            this.doSearch(searchData);
          }
        })

        this.authService.currentUserObservable.subscribe(user => {
          this.showCardStats = user && user.isAdmin ? true : false;
        })

        this.route.queryParams.subscribe(params => {
          // Defaults to 0 if no query param provided.
          this.shouldScrollToContent = params['from'] ? true : false;
          this.globalLayoutOptionsService.setMobileSearchVisibility(params['from'] ? true : false);
        });
      }
    }

  ngOnInit() {
    this.globalLayoutOptionsService.setMobileSearchVisibility(true);

    if (SharedConfig.instance().isServer) {
      let pets = this.injector.get('pets', null);
      if (pets) {
        let list = [];
        for (let p = 0; p < pets.length; p++) {
          const rawPet = pets[p];
          list.push(new Pet(rawPet.id, rawPet));
        }
        this.results = list;
        this.groupedResults = this.petService.groupResults(list, 3);
      }
    } else {
      $('div.bgParallax').each(function(){
        var $obj = $(this);
          $(window).scroll(function() {
            var yPos = -($(window).scrollTop() / $obj.data('speed')); 
            var bgpos = '50% '+ yPos + 'px';
            $obj.css('background-position', bgpos);
        }); 
      });

     
      
    }
  }

  ngOnDestroy() {
    if (this.searchSub)
      this.searchSub.unsubscribe();
    if (this.authSub)
      this.authSub.unsubscribe();
  }

  onHomeSearchSubmit(search) {
    this.doSearch(search, true);
  }

  private doSearch(search, forceScroll = false) {
    this.petService.searchPets(search).then(list => {
      this.results = list;
      this.groupedResults = this.petService.groupResults(list, 3);

      if (this.shouldScrollToContent || forceScroll) {
        $('html, body').animate({
          scrollTop: $("#list-animals").offset().top
        }, 500);
      }
    });
  }
}
