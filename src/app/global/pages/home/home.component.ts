import { Component, OnInit, OnDestroy, Injector } from '@angular/core';

import { Router } from '@angular/router';
import { GlobalLayoutOptionsService } from '../../../shared/services/global-layout-options.service';
import { SharedConfig } from '../../../shared/shared.config';
import { NewsletterSubscriber } from '../../models/newsletter-subscriber';
import { SubscriberService } from '../../../shared/services/subscriber.service';

declare var $;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  subscribedToNewsletter: boolean = false;
  newsletterData: NewsletterSubscriber = new NewsletterSubscriber(null);

  constructor(private router: Router,
    private globalLayoutOptionsService: GlobalLayoutOptionsService,
    private subscriberService: SubscriberService) { }

  ngOnInit() {
    this.globalLayoutOptionsService.setMobileSearchVisibility(true, false);

    if (SharedConfig.instance().isServer) {
      // let pets = this.injector.get('pets', null);
      // if (pets) {
      //   let list = [];
      //   for (let p = 0; p < pets.length; p++) {
      //     const rawPet = pets[p];
      //     list.push(new Pet(rawPet.id, rawPet));
      //   }
      //   this.results = list;
      //   this.groupedResults = this.petService.groupResults(list, 3);
      // }
    } else {
      $('div.bgParallax').each(function(){
        var $obj = $(this);
          $(window).scroll(function() {
            var yPos = -($(window).scrollTop() / $obj.data('speed')); 
            var bgpos = '50% '+ yPos + 'px';
            $obj.css('background-position', bgpos);
        }); 
      });

      $.scrollify({
        section : ".section-how-to",
        scrollbars:false,
        before:function(i,panels) {
          var ref = panels[i].attr("data-section-name"); 
          $(".pagination .active").removeClass("active");

          $(".pagination").find("a[href=\"#" + ref + "\"]").addClass("active");
          if(ref != 'search') {
            $('header').animate({
              top: -60,
            }, 500)
          } else {
            $('header').animate({
              top: 0
            }, 300)
          }
        },
        afterRender:function() {
          var pagination = "<ul id='home-pagination' class=\"pagination hide-mobile\">";
          var activeClass = "";
          $(".section-how-to").each(function(i) {
            activeClass = "";
            if(i===0) {
              activeClass = "active";
            }
            pagination += "<li><a class=\"" + activeClass + "\" href=\"#" + $(this).attr("data-section-name") + "\"><span class=\"hover-text\">" + $(this).attr("data-section-name").charAt(0).toUpperCase() + $(this).attr("data-section-name").slice(1) + "</span></a></li>";
          });

          pagination += "</ul>";

          $("body").append(pagination);
    
          $(".pagination a").on("click",$.scrollify.move);
        }

      });

      $('#back-to-top').on('click tap', function(){
        $.scrollify.move('#search');
      });
      $('#home-pagination').show();
    }
  }

  ngOnDestroy() { 
    if (!SharedConfig.instance().isServer) {
      $.scrollify.destroy();
      $('#home-pagination').hide();
    } 
  }

  onHomeSearchSubmit(search) {
    // this.doSearch(search);
    this.router.navigate(["/encontrar-melhor-amigo"], { queryParams: { from: 'home' } });
  }

  subscribeToNewsletter() {
    if (!this.newsletterData.email || this.newsletterData.email.length == 0)
      return;

    const component = this;
    this.globalLayoutOptionsService.setLoading(true);
    this.subscriberService.searchSubscribers(this.newsletterData.email).then(results => {
      if (results.length > 0) {
        component.globalLayoutOptionsService.setLoading(false);
        this.subscribedToNewsletter = true;
      } else {
        this.subscriberService.subscribe(this.newsletterData).then(() => {
          component.globalLayoutOptionsService.setLoading(false);
          this.subscribedToNewsletter = true;
        }, error => {
          component.globalLayoutOptionsService.setLoading(false);
        });
      }
    }, error => {
      component.globalLayoutOptionsService.setLoading(false);
    });
  }

  redirectToRoot() {
    this.router.navigate(["/"]);
  }
}
