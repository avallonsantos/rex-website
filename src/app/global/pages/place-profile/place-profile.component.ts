import { Component, OnInit, OnDestroy } from '@angular/core';

import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { PetService } from '../../../shared/services/pet.service';
import { PlaceService } from '../../../shared/services/place.service';
import { SharedConfig } from '../../../shared/shared.config';
import { AuthService } from '../../../shared/services/auth.service';
import { Place } from '../../models/place';
import { GlobalLayoutOptionsService } from '../../../shared/services/global-layout-options.service';

declare const $;

@Component({
  selector: 'app-place-profile',
  templateUrl: './place-profile.component.html',
  styleUrls: ['./place-profile.component.scss']
})
export class PlaceProfileComponent implements OnInit, OnDestroy {

  place;
  pets = [];

  constructor(private route: ActivatedRoute, private router: Router,
    private petService: PetService, private placeService: PlaceService,
    private authService: AuthService, private globalLayoutOptionsService: GlobalLayoutOptionsService) { }

  ngOnInit() {
    let place = this.route.data["place"];
    if (place) {
      this.setPlace(place);
    } else {
      this.route.data.subscribe((data: { place: Place }) => {
        if (data && !place) {
          this.setPlace(data.place);
        }
      });
    }

    this.globalLayoutOptionsService.setMobileSearchVisibility(false);

    if (!SharedConfig.instance().isServer) {
      var limit = ($('footer').offset().top) - 100;
    
      $('.navegation-active, .can-be-fixed').scrollToFixed({
        top: 100,
        limit: limit
      });
    
      $(document).on("scroll", this.onScroll);
    }
  }

  private initPetsView() {
    var quantAnimals = $('.slide-pets-full').children('.animal').length;
    $('.slide-pets-full').addClass('has-'+quantAnimals+'-slides');
    $('.slide-pets-full').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      centerMode: true,
      infinite: false,
      centerPadding: '80px',
      prevArrow: '<img class="prev-arrow" src="public/img/_icons/arrow-slide-left.svg">',
      nextArrow: '<img class="next-arrow" src="public/img/_icons/arrow-slide-right.svg">',
       responsive: [
          {
            breakpoint: 1350,
            settings: {
              centerMode: true,
              centerPadding: '60px',
              slidesToShow: 3
            }
          },
          {
            breakpoint: 768,
            settings: {
             centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
           },
          {
            breakpoint: 580,
            settings: {
             centerMode: true,
              centerPadding: '50px',
              slidesToShow: 1
            }
          }
        ]
    })
  }

  ngOnDestroy() {
  }

  setPlace(place: Place) {
    this.place = place;

    if (this.place) {
      this.onPlaceLoaded();

      if (!SharedConfig.instance().isServer) {
        const component = this;
        this.petService.loadPlacePets(this.place).then(pets => {
          this.pets = pets;

          setTimeout(() => {
            component.initPetsView();
          }, 100);
        })
      };
    }
  }

  onPlaceLoaded() {
    if (!SharedConfig.instance().isServer) {
      setTimeout(() => {
        $('.slide-pet ul').slick({
          autoplay: true,
          autoplaySpeed: 5000,
          prevArrow: '<img class="prev-arrow" src="public/img/_icons/arrow-slide-left.svg">',
          nextArrow: '<img class="next-arrow" src="public/img/_icons/arrow-slide-right.svg">',
        });
      }, 50);
    }
  }

  private onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('.navegation-active li .page-local').each(function () {
      var currLink = $(this);
      var refElement = $(currLink.attr("data-target"));
      if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
          $('.navegation-active li .page-local').removeClass("active");
          currLink.addClass("active");
      }
      else {
          currLink.removeClass("active");
      }
    });
  }

  onSearchSubmit(search) {
    this.router.navigate(["/encontrar-melhor-amigo"], { queryParams: { from: 'place' } });
  }

}
