import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { trigger, state, animate, transition, style } from '@angular/animations';

@Component({
  selector : 'app-sub-menu',
  animations: [
    trigger('visibilityChanged', [
      state('shown' , style({ display: "block" })),
      state('hidden', style({ display: "none" })),
      transition('* => *', animate('.2s'))
    ])
  ],
  template: `<ul class="sub-menu" [@visibilityChanged]="visibility"><ng-content></ng-content></ul>`
})
export class SubMenuComponent implements OnChanges {
  @Input() isVisible : boolean = true;
  @Output() onVisibilityChanged = new EventEmitter;
  visibility = 'hidden';

  ngOnChanges() {
   this.visibility = this.isVisible ? 'shown' : 'hidden';
   this.onVisibilityChanged.emit(this.visibility);
  }
}