import { Component, OnInit, OnChanges, OnDestroy, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { SharedConfig } from '../../shared/shared.config';
import { AuthService } from '../../shared/services/auth.service';
import { PlaceService } from '../../shared/services/place.service';
import { SearchStateService } from '../../shared/services/search-state.service';
import { GlobalLayoutOptionsService } from '../../shared/services/global-layout-options.service';

declare var $;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnChanges, OnDestroy {

  @Output() toggleSidebarTriggered= new EventEmitter;

  @Input() mobileSearch: boolean = false;
  @Output() mobileSearchChange: EventEmitter<boolean> = new EventEmitter;
  mobileSearchSub;

  @Input() mobileMenuAnchor: boolean = false;
  mobileMenuAnchorSearchSub;

  @Input() mobileProfile: boolean = false;
  @Output() mobileProfileChange: EventEmitter<boolean> = new EventEmitter;
  @Input() mobileVisitor: boolean = false;
  @Output() mobileVisitorChange: EventEmitter<boolean> = new EventEmitter;
  @Input() mobilePlaceConfig: boolean = false;
  @Output() mobilePlaceConfigChange: EventEmitter<boolean> = new EventEmitter;

  @Input() currentUser:any;
  @Output() loginTriggered= new EventEmitter;
  currentPlace;

  isServer = SharedConfig.instance().isServer;
  submenusVisibility:any = {};

  constructor(private authService: AuthService, 
    private placeService: PlaceService,
    private searchStateService: SearchStateService,
    private router: Router, 
    private globalLayoutOptionsService: GlobalLayoutOptionsService) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.mobileSearchSub = this.globalLayoutOptionsService.mobileSearchVisibilityObservable.subscribe(visible => {
        if (this.mobileSearch != visible) {
          this.mobileSearch = visible;
          this.checkNeedsHTMLOpened();
        }
      })

      this.mobileMenuAnchorSearchSub = this.globalLayoutOptionsService.mobileMenuAnchorSearchVisibilityObservable.subscribe(visible => {
        if (this.mobileMenuAnchor != visible) {
          this.mobileMenuAnchor = visible;
        }
      })
    }
  }

  ngOnChanges() {
    if (this.currentUser) {
      this.placeService.listByUser(this.currentUser).then(places => {
        if (places && places.length > 0) {
          this.currentPlace = places[0];
        } else {
          this.currentPlace = null;
        }
      })
    } else {
      this.currentPlace = null;
    }
  }

  ngOnDestroy() {
    if (this.mobileSearchSub)
      this.mobileSearchSub.unsubscribe();
    
    if (this.mobileMenuAnchorSearchSub)
      this.mobileMenuAnchorSearchSub.unsubscribe();
  }

  toggleSibebar() {
    this.toggleSidebarTriggered.emit();
  }

  toggleMobileSearchMenu() {
    this.setMobileSearchMenu(!this.mobileSearch);
  }

  toggleMobileProfileMenu() {
    this.setMobileProfileMenu(!this.mobileProfile);
  }

  toggleMobileVisitorMenu() {
    this.setMobileVisitorMenu(!this.mobileVisitor)
  }

  setMobileSearchMenu(mobileSearch) {
    this._setMobileSearchMenu(mobileSearch);
    this._setMobileProfileMenu(false);
    this._setMobileVisitorMenu(false);
    this.checkNeedsHTMLOpened();
  }

  setMobileProfileMenu(mobileProfile) {
    this._setMobileProfileMenu(mobileProfile);
    this._setMobileSearchMenu(false);
    this._setMobileVisitorMenu(false);
    this.checkNeedsHTMLOpened();
  }

  setMobileVisitorMenu(mobileVisitor) {
    this._setMobileVisitorMenu(mobileVisitor);
    this._setMobileSearchMenu(false);
    this._setMobileProfileMenu(false);
    this.checkNeedsHTMLOpened();
  }

  private _setMobileSearchMenu(mobileSearch) {
    this.mobileSearch = mobileSearch;
    this.mobileSearchChange.emit(this.mobileSearch);
    this.globalLayoutOptionsService.setMobileSearchVisibility(this.mobileSearch, this.mobileMenuAnchor);
  }

  private _setMobileProfileMenu(mobileProfile) {
    this.mobileProfile = mobileProfile;
    this.mobileProfileChange.emit(this.mobileProfile);
  }

  private _setMobileVisitorMenu(mobileVisitor) {
    this.mobileVisitor = mobileVisitor;
    this.mobileVisitorChange.emit(this.mobileVisitor);
  }

  toggleMobilePlaceConfigSubMenu() {
    this.mobilePlaceConfig = !this.mobilePlaceConfig;
    this.mobilePlaceConfigChange.emit(this.mobilePlaceConfig);
    this.checkNeedsHTMLOpened();
  }

  private checkNeedsHTMLOpened() {
    // if (this.mobileVisitor || this.mobileProfile || this.mobilePlaceConfig || this.mobileSearch) {
    if (this.mobileProfile) {
      if (!$('html').hasClass('opened')) {
        $('html').addClass('opened')
      }
    } else {
      if ($('html').hasClass('opened')) {
        $('html').removeClass('opened')
      }
    }

    $(document).on('scroll', function(){
      var topScroll = $(this).scrollTop();
      if(topScroll >= $('.ads').offset().top) {
        $('#fixed-mob-search .page-top').removeClass('opened').addClass('fixed');
      } else {
        $('#fixed-mob-search .page-top').removeClass('fixed').addClass('opened');
      }
    })
  }

  closeAllMobileMenus() {
    this._setMobileSearchMenu(false);
    this._setMobileProfileMenu(false);
    this._setMobileVisitorMenu(false);
  }

  login() {
    this.closeAllMobileMenus();
    this.loginTriggered.emit();
  }

  logout() {
    this.authService.logout().then(() => {
      this.closeAllMobileMenus();
      this.router.navigate(["/"]);
    })
  }

}
