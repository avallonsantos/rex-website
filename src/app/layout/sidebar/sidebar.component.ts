import { Component, AfterViewInit, OnChanges, ElementRef } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { SharedConfig } from '../../shared/shared.config';
import { AuthService } from '../../shared/services/auth.service';
import { PlaceService } from '../../shared/services/place.service';

declare var PerfectScrollbar;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements AfterViewInit, OnChanges {

  @Input() sidebarVisible:boolean = false;
  @Output() sidebarShown = new EventEmitter;
  @Output() sidebarHidden = new EventEmitter;

  @Input() currentUser:any;
  currentPlace;

  constructor(private elRef:ElementRef,
    private authService: AuthService,
    private placeService: PlaceService,
    private router: Router) { }

  ngAfterViewInit() {
    if (!SharedConfig.instance().isServer) {
      const ps = new PerfectScrollbar('#sidebar-menu');
    }
  }

  ngOnChanges() {
    if (this.sidebarVisible)  {
      this.showSidebar();
    } else {
      this.hideSidebar();
    }

    if (this.currentUser) {
      this.placeService.listByUser(this.currentUser).then(places => {
        if (places && places.length > 0) {
          this.currentPlace = places[0];
        } else {
          this.currentPlace = null;
        }
      })
    } else {
      this.currentPlace = null;
    }
  }

  logout() {
    this.authService.logout().then(() => {
      this.hideSidebar();
      this.router.navigate(["/"]);
    })
  }

  showSidebar() {
    // $('#sidebar-menu').toggleClass('opened');
    // $('.mask-page').fadeToggle('fast');
    this.sidebarShown.emit();
  }

  hideSidebar() {
    // $('#sidebar-menu').removeClass('opened');
    // $('.mask-page').fadeOut('fast');
    if (this.sidebarVisible)
      this.sidebarVisible = false;

    this.sidebarHidden.emit();
  }

}
