import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Place } from '../../../global/models/place';
import { UserService } from '../../../shared/services/user.service';

@Component({
  selector: 'admin-place-card',
  templateUrl: './place-card.component.html',
  styleUrls: ['./place-card.component.scss']
})
export class PlaceCardComponent implements OnInit {

  @Input() place:Place;
  userOwner;
  
  @Output() editClicked = new EventEmitter; 
  @Output() openClicked = new EventEmitter; 

  editMenuVisible:boolean = false;
  menuClicked:boolean = false;
  
  constructor(private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    this.editMenuVisible = false;
    this.menuClicked = false;
    // $('.edit-trigger').on('click', function(){
    //   $(this).next('.menu-fast-edit').toggleClass('opened');
    // });
  }

  ngOnChanges() {
    if (!this.userOwner && this.place && this.place.user) {
      this.userService.retrieveFromObject(this.place.user).then(user => {
        this.userOwner = user;
      });
    }
  }

  open() {
    this.router.navigate(["/place", this.place.getOrCalculateSlug()])
    this.openClicked.emit(this.place);
  }

  openEditor() {
    this.router.navigate(["/conta/configurar-empresa", this.place.getOrCalculateSlug()])
    this.editClicked.emit(this.place);
    this.editMenuVisible = false;
  }

  toggleEditMenu() {
    this.editMenuVisible = this.editMenuVisible ? false :  true;
    
    const component = this;
    component.menuClicked = true;
    setTimeout(() => {
      component.menuClicked = false;
    })
  }

  closeMenuIfVisible() {
    if (!this.menuClicked)
      this.editMenuVisible = false;
  }

}
