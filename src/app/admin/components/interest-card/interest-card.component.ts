import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Interest } from '../../../global/models/interest';
import { Pet } from '../../../global/models/pet';
import { User } from '../../../global/models/user';
import { UserService } from '../../../shared/services/user.service';
import { PetService } from '../../../shared/services/pet.service';
import { PlaceService } from '../../../shared/services/place.service';
import { InterestService } from '../../../shared/services/interest.service';

@Component({
  selector: 'admin-interest-card',
  templateUrl: './interest-card.component.html',
  styleUrls: ['./interest-card.component.scss']
})
export class InterestCardComponent implements OnInit {

  @Input() interest:Interest;
  user;
  pet;
  place;

  ratingForm: any = { rating: 0 };
  availableRatings = [1,2,3,4,5];
  
  @Output() editClicked = new EventEmitter; 
  @Output() openClicked = new EventEmitter; 

  editMenuVisible:boolean = false;
  menuClicked:boolean = false;
  
  constructor(private router: Router,
    private userService: UserService,
    private petService: PetService,
    private placeService: PlaceService,
    private interestService: InterestService) { }

  ngOnInit() {
    this.editMenuVisible = false;
    this.menuClicked = false;
    // $('.edit-trigger').on('click', function(){
    //   $(this).next('.menu-fast-edit').toggleClass('opened');
    // });
  }

  ngOnChanges() {
    if (!this.user && this.interest && this.interest.user) {
      this.userService.retrieveFromObject(this.interest.user).then(user => {
        this.user = user;
      });
    }

    if (!this.pet && this.interest && this.interest.pet) {
      this.petService.retrieveFromObject(this.interest.pet).then(pet => {
        this.pet = pet;
        if (this.pet) {
          this.placeService.retrieveFromObject(this.pet.place).then(place => {
            this.place = place;
          });
        }
      });
    }
  }

  submitRating() {
    console.log(this.ratingForm.rating);
    if (this.ratingForm.rating > 0 && (!this.interest.rating || this.interest.rating < 0)) {
      this.interestService.rate(this.interest, this.ratingForm.rating).then(() => {
        this.interest.rating = this.ratingForm.rating;
      })
    }
  }

}
