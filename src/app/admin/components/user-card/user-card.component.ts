import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../../global/models/user';
import { Place } from '../../../global/models/place';
import { PlaceService } from '../../../shared/services/place.service';

@Component({
  selector: 'admin-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  @Input() user:User;
  ownedPlace: Place;
  
  @Output() editClicked = new EventEmitter; 
  @Output() openClicked = new EventEmitter; 

  editMenuVisible:boolean = false;
  menuClicked:boolean = false;
  
  constructor(private router: Router,
    private placeService: PlaceService) { }

  ngOnInit() {
    this.editMenuVisible = false;
    this.menuClicked = false;
    // $('.edit-trigger').on('click', function(){
    //   $(this).next('.menu-fast-edit').toggleClass('opened');
    // });
  }

  open() {
    this.router.navigate(["/user", this.user.id])
    this.openClicked.emit(this.user);
  }

  openPEditor() {
    this.router.navigate(["/configurar", this.user.id])
    this.editClicked.emit(this.user);
    this.editMenuVisible = false;
  }

  ngOnChanges() {
    if (!this.ownedPlace && this.user && this.user.ownedPlace) {
      this.placeService.retrieveFromObject(this.user.ownedPlace).then(place => {
        this.ownedPlace = place;
      });
    }
  }

  toggleEditMenu() {
    this.editMenuVisible = this.editMenuVisible ? false :  true;
    
    const component = this;
    component.menuClicked = true;
    setTimeout(() => {
      component.menuClicked = false;
    })
  }

  closeMenuIfVisible() {
    if (!this.menuClicked)
      this.editMenuVisible = false;
  }

}
