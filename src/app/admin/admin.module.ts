import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UserListComponent } from './pages/user-list/user-list.component';
import { PlaceListComponent } from './pages/place-list/place-list.component';
import { InterestListComponent } from './pages/interest-list/interest-list.component';
import { SharedModule } from '../shared/shared.module';
import { EditingModule } from '../editing/editing.module';

import { AdminRouter } from '../admin/admin.router';
import { UserCardComponent } from './components/user-card/user-card.component';
import { PlaceCardComponent } from './components/place-card/place-card.component';
import { InterestCardComponent } from './components/interest-card/interest-card.component';
import { SubscribersListComponent } from './pages/subscribers-list/subscribers-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    AdminRouter,
    EditingModule,
  ],
  declarations: [UserCardComponent, UserListComponent, PlaceListComponent, PlaceCardComponent, InterestListComponent, InterestCardComponent, SubscribersListComponent]
})
export class AdminModule { }
