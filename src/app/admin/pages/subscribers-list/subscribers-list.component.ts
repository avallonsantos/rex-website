import { Component, OnInit } from '@angular/core';
import { SubscriberService } from '../../../shared/services/subscriber.service';
import { SharedConfig } from '../../../shared/shared.config';

@Component({
  selector: 'app-subscribers-list',
  templateUrl: './subscribers-list.component.html',
  styleUrls: ['./subscribers-list.component.scss']
})
export class SubscribersListComponent implements OnInit {

  list: any;

  constructor(private subscriberService: SubscriberService) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.loadAll();
    }
  }

  loadAll() {
    this.subscriberService.loadAll().then(results => {
      this.list = results;
    })
  }

  remove(item) {
    if (!confirm("Tem certeza que deseja remover?"))
      return;

    this.subscriberService.remove(item).then(() => {
      this.loadAll();
    })
  }

}
