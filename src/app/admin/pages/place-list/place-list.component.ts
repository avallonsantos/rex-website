import { Component, OnInit } from '@angular/core';
import { SharedConfig } from '../../../shared/shared.config';
import { PlaceService } from '../../../shared/services/place.service';
import { groupList } from '../../../global/utils/lists';

@Component({
  selector: 'app-place-list',
  templateUrl: './place-list.component.html',
  styleUrls: ['./place-list.component.scss']
})
export class PlaceListComponent implements OnInit {

  results:any;
  groupedResults:any;

  constructor(private placeService: PlaceService) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.doSearch({ });
    }
  }

  private doSearch(search) {
    this.placeService.search(search).then(list => {
      this.results = list;
      this.groupedResults = groupList(list, 3);
    });
  }

}
