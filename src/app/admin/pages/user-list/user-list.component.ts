import { Component, OnInit } from '@angular/core';
import { SharedConfig } from '../../../shared/shared.config';
import { groupList } from '../../../global/utils/lists';
import { UserService } from '../../../shared/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  results:any;
  groupedResults:any;

  constructor(private userService: UserService) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.doSearch({ });
    }
  }

  private doSearch(search) {
    this.userService.search(search).then(list => {
      this.results = list;
      this.groupedResults = groupList(list, 3);
    });
  }

}
