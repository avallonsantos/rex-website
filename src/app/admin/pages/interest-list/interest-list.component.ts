import { Component, OnInit } from '@angular/core';
import { SharedConfig } from '../../../shared/shared.config';
import { InterestService } from '../../../shared/services/interest.service';
import { groupList } from '../../../global/utils/lists';

@Component({
  selector: 'app-interest-list',
  templateUrl: './interest-list.component.html',
  styleUrls: ['./interest-list.component.scss']
})
export class InterestListComponent implements OnInit {

  results:any;
  groupedResults:any;

  constructor(private interestService: InterestService) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.doSearch({ });
    }
  }

  private doSearch(search) {
    this.interestService.search(search).then(list => {
      this.results = list;
      this.groupedResults = groupList(list, 3);
    });
  }

}
