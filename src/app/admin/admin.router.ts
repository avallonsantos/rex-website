import { Routes, RouterModule } from '@angular/router';
import {
    PlaceListComponent,
    UserListComponent,
    InterestListComponent,
    SubscribersListComponent
} from './admin.pages';

const ADMIN_ROUTER: Routes = [
    {
        path: '',
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'usuarios'
            },
            {
                path: 'empresas',
                component: PlaceListComponent,
            },
            {
                path: 'interesses',
                component: InterestListComponent,
            },
            {
                path: 'assinantes',
                component: SubscribersListComponent,
            },
            {
                path: 'usuarios',
                component: UserListComponent,
            }
        ]
    },
    {
        path: '**',
        redirectTo: '/'
    }
];

export const AdminRouter = RouterModule.forChild(ADMIN_ROUTER);

