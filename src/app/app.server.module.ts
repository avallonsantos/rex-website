import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import {ModuleMapLoaderModule} from '@nguniversal/module-map-ngfactory-loader';

import * as firebase from '@firebase/app'; 
import { AngularFireModule } from 'angularfire2';
import { AppModule  } from "./app.module";
import { AppComponent  } from "./app.component";
import { environment } from '../environments/environment';

@NgModule({
    imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AppModule,
        ServerModule,
        ServerTransferStateModule,
        ModuleMapLoaderModule,
    ],
    bootstrap: [AppComponent],
    providers: [ ]
  })
export class AppServerModule { }