import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';

import { TextMaskModule } from 'angular2-text-mask';

// Routing
import { appRouter } from './app.router';
import { Router } from '@angular/router';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { environment } from '../environments/environment';

// Child Modules
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { HomeComponent, RegisterPlaceComponent, FindFriendComponent } from './global/pages/public';
import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';

import { SubMenuComponent } from './layout/header/sub-menu/sub-menu.component';
import { RegisterUserComponent } from './global/pages/register-user/register-user.component';
import { PetProfileComponent } from './global/pages/pet-profile/pet-profile.component';
import { PlaceProfileComponent } from './global/pages/place-profile/place-profile.component';
import { ContactComponent } from './global/pages/contact/contact.component';
import { ManifestComponent } from './global/pages/manifest/manifest.component';
import { TermsComponent } from './global/pages/terms/terms.component';

import { GlobalLayoutOptionsService } from './shared/services/global-layout-options.service';
import { AuthService } from './shared/services/auth.service';

// import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
// import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
// import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
// const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = { };

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FindFriendComponent,
    RegisterPlaceComponent,
    HeaderComponent,
    SidebarComponent,
    SubMenuComponent,
    RegisterUserComponent,
    PetProfileComponent,
    PlaceProfileComponent,
    ContactComponent,
    ManifestComponent,
    TermsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "rex-website" }),
    BrowserAnimationsModule,
    BrowserTransferStateModule,
    FormsModule,
    HttpModule,
    appRouter,
    SharedModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    TextMaskModule,
    // PerfectScrollbarModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    AuthService,
    GlobalLayoutOptionsService
    // { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
