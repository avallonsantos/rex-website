import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TextMaskModule } from 'angular2-text-mask';

import { PetCardComponent } from './components/pet-card/pet-card.component';
import { RatingReadonlyComponent } from './components/rating-readonly/rating-readonly.component';
import { GalleryPickerComponent } from './components/gallery-picker/gallery-picker.component';

import { OwnerTypePipe } from './pipes/owner-type.pipe';
import { OwnerTypeNamePipe } from './pipes/owner-type-name.pipe';
import { AddressToAreaPipe } from './pipes/address-to-area.pipe';

import { PetService, PetResolver } from './services/pet.service';
import { OfferTypeNamePipe } from './pipes/offer-type-name.pipe';
import { MainContainerComponent } from './components/main-container/main-container.component';
import { SelectizeComponent } from './components/selectize/selectize.component';
import { FormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { EditingModule } from '../editing/editing.module';
import { LoginComponent } from './components/login/login.component';
import { ImagePickerComponent } from './components/image-picker/image-picker.component';
import { PlaceService, PlaceResolver } from './services/place.service';
import { GlobalLayoutOptionsService } from './services/global-layout-options.service';
import { PedigreeToStringPipe } from './pipes/pedigree-to-string.pipe';
import { PetSearchComponent } from './components/pet-search/pet-search.component';
import { SearchStateService } from './services/search-state.service';
import { AnalyticsService } from './services/analytics.service';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { WeekDayNamePipe } from './pipes/week-day-name.pipe';
import { AdminGuard } from './guards/admin.guard';
import { PlaceFormComponent } from './components/place-form/place-form.component';
import { InterestService } from './services/interest.service';
import { ContactService } from './services/contact.service';
import { SubscriberService } from './services/subscriber.service';
import { FooterComponent } from '../layout/footer/footer.component';
import { PopupComponent } from './components/popup/popup.component';
import { PetSizeNamePipe } from './pipes/pet-size-name.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    EditingModule,
    RouterModule,
    TextMaskModule
  ],
  declarations: [
    PetCardComponent, 
    RatingReadonlyComponent,
    GalleryPickerComponent,
    OwnerTypePipe,
    OwnerTypeNamePipe,
    OfferTypeNamePipe,
    MainContainerComponent,
    SelectizeComponent,
    LoginComponent,
    ImagePickerComponent,
    AddressToAreaPipe,
    PedigreeToStringPipe,
    PetSearchComponent,
    ClickOutsideDirective,
    WeekDayNamePipe,
    PlaceFormComponent,
    FooterComponent,
    PopupComponent,
    PetSizeNamePipe,
  ],
  exports: [
    PetCardComponent, 
    RatingReadonlyComponent,
    GalleryPickerComponent,
    OwnerTypePipe,
    OwnerTypeNamePipe,
    OfferTypeNamePipe,
    PetSizeNamePipe,
    MainContainerComponent,
    SelectizeComponent,
    LoginComponent,
    ImagePickerComponent,
    AddressToAreaPipe,
    PedigreeToStringPipe,
    PetSearchComponent,
    ClickOutsideDirective,
    WeekDayNamePipe,
    PlaceFormComponent,
    FooterComponent,
    PopupComponent,
  ],
  providers: [PetService, PetResolver, UserService, PlaceService, PlaceResolver, SearchStateService, AnalyticsService, AdminGuard, InterestService, ContactService, SubscriberService]
})
export class SharedModule { }
