import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GlobalLayoutOptionsService } from '../../services/global-layout-options.service';

@Component({
  selector: 'shared-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  @Input() popupId: string = "";
  @Input() extraClass: string = "";


  @Input() popupVisible : boolean = true;

  _popupShown: boolean = false;
  @Input()
  set popupShown(popupShown: boolean) {
    this._popupShown = popupShown;
    this.popupShownChange.emit(popupShown);
    this.checkPopupShown();
  }
  get popupShown(): boolean { return this._popupShown; }
  @Output() popupShownChange = new EventEmitter<boolean>();

  @Output() popupClosing = new EventEmitter<boolean>();

  constructor(private globalLayoutOptionsService: GlobalLayoutOptionsService) { }

  ngOnInit() {
  }

  checkPopupShown() {
    this.globalLayoutOptionsService.setMaskPageVisibility(this.popupShown);
  }

  showPopup() {
    this.popupShown = true;
  }

  private _closePopup() {
    this.popupShown = false;
  }

  closePopup() {
    this._closePopup();
    this.popupClosing.emit();
  }

  clickedOutsidePopup() {
    this._closePopup();
  }

}
