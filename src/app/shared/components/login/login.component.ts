import { Component, OnInit, ViewChild } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { GlobalLayoutOptionsService } from '../../services/global-layout-options.service';
import { SharedConfig } from '../../shared.config';

declare var $;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @ViewChild('loginForm') loginForm;

  private _openedChanged:boolean;
  private _opened:boolean;
  @Input()
  set opened(opened: any) {
    this._openedChanged = true;
    this._opened = opened;
    this.openedChange.emit(this.opened);

    const component = this;
    setTimeout(() => {
      component._openedChanged = false;
    })
    this.checkAfterLoginChanged();
  }
  get opened(): any { return this._opened; }
  @Output() openedChange = new EventEmitter<boolean>();

  @Output() onLoginExit: EventEmitter<any> = new EventEmitter();
  @Output() onLogin: EventEmitter<any> = new EventEmitter();

  loginData = { email: "", password: ""};

  constructor(private router: Router,
    private authService: AuthService,
    private globalLayoutOptionsService: GlobalLayoutOptionsService) { }

  ngOnInit() {
  }

  private checkAfterLoginChanged() {
    if (!SharedConfig.instance().isServer) {
      let body = $("body");
      if (this._opened) {
        if (!body.hasClass('fixed'))
          body.addClass('fixed');
      } else {
        if (body.hasClass('fixed'))
          body.removeClass('fixed');
      }
    }
  }

  close() {
    if (!this._openedChanged) {
      this.opened = false;
      this.onLoginExit.emit();
    }
  }

  register() {
    this.router.navigate(["/registrar"]);
    this.opened = false;
    this.onLoginExit.emit();
  }

  handleError(error) {
    this.globalLayoutOptionsService.setLoading(false);
  }

  loginWithFacebook() {
    const component = this;
    this.globalLayoutOptionsService.setLoading(true);
    this.authService.loginWithFacebook().then(result => {
      this.globalLayoutOptionsService.setLoading(false);
      this.opened = false;
      this.onLogin.emit(result);
    }, error => {
      component.handleError(error);
    });
  }

  loginWithGoogle() {
    const component = this;
    this.globalLayoutOptionsService.setLoading(true);
    this.authService.loginWithGoogle().then(result => {
      this.globalLayoutOptionsService.setLoading(false);
      this.opened = false;
      this.onLogin.emit(result);
    }, error => {
      component.handleError(error);
    });
  }

  loginManual() {
    if (!this.loginForm.form.valid)
      return;

    const component = this;
    this.globalLayoutOptionsService.setLoading(true);
    this.authService.loginManual(this.loginData.email, this.loginData.password).then(result => {
      this.globalLayoutOptionsService.setLoading(false);
      this.opened = false;
      this.onLogin.emit(result);
    }, error => {
      component.handleError(error);
    });
  }
}
