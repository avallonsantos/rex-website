import { Component, OnInit, Input, Output, AfterViewInit, EventEmitter, OnDestroy } from '@angular/core';
import { SearchData, PetService } from '../../services/pet.service';
import { addDefaultToList, addDefaultToStringList } from '../../../global/utils/lists';

import { YEAR_LIST, YEAR_RANGE_LIST, MONTH_LIST, 
  TYPE_LIST, OFFER_TYPE_LIST, 
  GENDER_LIST, SIZES_LIST
} from '../../../global/models/pet';
import { STATES } from '../../../global/models/state';
import { SearchStateService } from '../../services/search-state.service';
import { SharedConfig } from '../../shared.config';
import { GlobalLayoutOptionsService } from '../../services/global-layout-options.service';

@Component({
  selector: 'app-pet-search',
  templateUrl: './pet-search.component.html',
  styleUrls: ['./pet-search.component.scss']
})
export class PetSearchComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() alwaysVisible:boolean = false;
  @Input() showLabels:boolean = true;
  @Input() hideSmallOnMobile:boolean = false;

  @Input() includeCoverImage: boolean = false;
  @Input() extraClass: string = "";

  @Output() onSearch:EventEmitter<SearchData> = new EventEmitter;

  search:SearchData = { 
    type: { dog: false, cat: false },
    place_type: { canil: false, ong: false },
    breed: null,
    age_range: null,
    size: null,
    place_state: null,
  };
  searchSub:any;

  yearList = addDefaultToList(YEAR_LIST, "-", "");
  yearRangeList = addDefaultToList(YEAR_RANGE_LIST, "-", "");
  sizeList = addDefaultToList(SIZES_LIST, "-", "");
  stateList = addDefaultToList(STATES, "-", "");

  breedList = [ ];

  mobileSearchVisibility: boolean = false;
  mobileSearchVisibilitySub;

  constructor(private petService: PetService,
    private searchStateService: SearchStateService,
    private globalLayoutOptionsService: GlobalLayoutOptionsService) { }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.searchSub = this.searchStateService.searchObservable.subscribe(searchData => {
        if (this.search != searchData) {
          this.search = searchData;
        }
      })

      this.mobileSearchVisibilitySub = this.globalLayoutOptionsService.mobileSearchVisibilityObservable.subscribe(visible => {
        this.mobileSearchVisibility = visible;
      });
    }
  }

  ngAfterViewInit() {
    if (!SharedConfig.instance().isServer) {
      this.loadBreeds();
    }
  }

  ngOnDestroy() {
    if (this.searchSub)
      this.searchSub.unsubscribe();

    if (this.mobileSearchVisibilitySub)
      this.mobileSearchVisibilitySub.unsubscribe();
  }

  onTypeChange() {
    this.loadBreeds();
  }

  loadBreeds() {
    const promises = [];
    const neitherSelected = !this.search.type.dog && !this.search.type.cat;
    if (this.search.type.dog || neitherSelected) {
      promises.push(this.petService.usedBreeds("dog"));
    }
    if (this.search.type.cat || neitherSelected) {
      promises.push(this.petService.usedBreeds("cat"));
    }
    if (promises.length > 0) {
      Promise.all(promises).then(results => {
        let list = [];
        for (let index = 0; index < results.length; index++) {
          list = list.concat(results[index]);
        }
        this.breedList = addDefaultToStringList(list, "-");
      });
    } else {
      this.breedList = [ ]
    }
  }

  onSearchSubmit() {
    this.searchStateService.setSearch(this.search);
    this.onSearch.emit(this.search);
  }

}
