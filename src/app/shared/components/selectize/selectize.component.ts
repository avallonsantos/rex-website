import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { ElementRef } from '@angular/core';

import { PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { findFirst } from '../../../global/utils/lists';

declare var $;

@Component({
  selector: 'shared-selectize',
  styleUrls: ['./selectize.component.scss'],
  template: `<select #mySelect name="{{selectName}}" id="{{selectName}}" class="{{selectClass}}">
      <option *ngFor="let state of options" [ngValue]="state.value">{{state.text}}</option>
    </select>`
})
export class SelectizeComponent implements OnInit, AfterViewInit {


  @ViewChild('mySelect') mySelect:ElementRef;
  mySelectize:any;

  private _optionsLoaded:boolean = false;
  private _options:any;
  @Input("options")
  set options(options: any) {
    this._options = options;
    this.onOptionsChanged();
  }
  get options(): any { return this._options; }

  private _namedOptions:any;
  @Input("namedOptions")
  set namedOptions(namedOptions: any) {
    this._namedOptions = namedOptions;
    this.options = namedOptions ? namedOptions.map(val => {
      return { code: val, name: val }
    }) : [];
  }
  get namedOptions(): any { return this._options; }

  @Input("config") config:any;
  @Input("selectClass") selectClass:string = '';
  @Input("selectName") selectName:string = '';
  @Input("label") label:string = '';

  private _selected:any;
  @Input()
  set selected(selected: any) {
    this._selected = selected;
    this.selectedChange.emit(this._selected);
  }
  get selected(): any { return this._selected; }
  @Output() selectedChange = new EventEmitter<any>();

  @Output() onEnter= new EventEmitter;

  constructor(@Inject(PLATFORM_ID) private platformId: Object) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.mySelectize = this.initSelectize();
      if (!this._optionsLoaded && this.options && this.options.length > 0) {
        this.onOptionsChanged();
      }
      if (this._selected)
        this.mySelectize.setValue(this._selected);
    }
  }

  initSelectize() {
    const component = this;
    this.config = this.config || { };
    if (!this.config.onEnter) {
      let onEnterCallback = () => { return component.onEnter.emit(); }
      this.config.onEnter = onEnterCallback;
    }
    const defaultOnChange = this.config.onChange;
    let onChangeCallback = (value) => { 
      var selectedItem = findFirst(this._options, item => {
        return item[this.config.valueField] && item[this.config.valueField].toString() == value;
      })
      this.selected = selectedItem[this.config.valueField];
      if (defaultOnChange) {
        defaultOnChange(value);
      }
      $('.selectize-dropdown-content .option').removeClass('selected');
      $(this).addClass('selected');
    }
    this.config.onChange = onChangeCallback;
    if (!this.config.valueField) this.config.valueField = "code";
    if (!this.config.labelField) this.config.labelField = "name";
    if (!this.config.placeholder) this.config.placeholder = this.label;

    $('.selectize-input input').attr('readonly', 'readonly');

    var selects = $(this.mySelect.nativeElement).selectize(this.config);
    for (let index = 0; index < selects.length; index++) {
      const selectize = selects[index].selectize;
      let onKeyDown = selectize.onKeyDown.bind(selectize);
      selectize.onKeyDown = function(e) {
        var result = onKeyDown(e)
        if (e.keyCode == 13) {
          this.settings.onEnter()
        }
        return result;
      }.bind(selectize)
    }

    return selects[0].selectize;


  }

  onOptionsChanged() {
    if (this.mySelectize) {
      this._optionsLoaded = true;
      this.mySelectize.clearOptions();
      if (this._options) {
        for (let index = 0; index < this._options.length; index++) {
          this.mySelectize.addOption(this._options[index]);
        }
      }
    } else {
      this._optionsLoaded = false;
    }
  }

}
