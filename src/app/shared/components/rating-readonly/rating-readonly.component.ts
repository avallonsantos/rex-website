import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'shared-rating-readonly',
  styleUrls: ['./rating-readonly.component.scss'],
  template: `<div class="rating">
      <span [class.full]="rating > 0">☆</span>
      <span [class.full]="rating > 1">☆</span>
      <span [class.full]="rating > 2">☆</span>
      <span [class.full]="rating > 3">☆</span>
      <span [class.full]="rating > 4">☆</span>
    </div>`
})
export class RatingReadonlyComponent implements OnInit {

  ratingArray:any[];
  private _rating:any;
  
   @Input()
   set rating(rating: number) {
     this._rating = rating;
     if (rating) {
      this.ratingArray = Array(rating);
     } else {
      this.ratingArray = [];
     }
   }
   get rating(): number { return this._rating; }

  constructor() { }

  ngOnInit() { }

}
