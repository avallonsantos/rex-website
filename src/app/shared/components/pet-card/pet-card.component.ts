import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Pet } from '../../../global/models/pet';
import { AnalyticsService } from '../../services/analytics.service';
import { SharedConfig } from '../../shared.config';
import { PetService } from '../../services/pet.service';
import { GlobalLayoutOptionsService } from '../../services/global-layout-options.service';

@Component({
  selector: 'shared-pet-card',
  templateUrl: './pet-card.component.html',
  styleUrls: ['./pet-card.component.scss']
})
export class PetCardComponent implements OnInit {

  @Input() pet:Pet;
  @Input() allowEdit:any = false;
  
  private _forceLoadStatsChanged:boolean;
  @Input()
  set forceLoadStats(forceLoadStats: any) {
    this._forceLoadStatsChanged = forceLoadStats;
    this.checkForceLoadStats();
  }
  get forceLoadStats(): any { return this._forceLoadStatsChanged; }

  @Input() allowStats:any = false;
  @Input() userStat;
  @Input() petStat;
  
  @Output() editClicked = new EventEmitter; 
  @Output() openClicked = new EventEmitter; 
  @Output() petRemoved = new EventEmitter; 

  editMenuVisible:boolean = false;
  menuClicked:boolean = false;

  popup_opened: boolean = false;
  show_remove: boolean = false;
  
  constructor(private router: Router,
    private petService: PetService,
    private globalLayoutOptionsService: GlobalLayoutOptionsService,
    private analyticsService: AnalyticsService) { }

  ngOnInit() {
    this.editMenuVisible = false;
    this.menuClicked = false;
    // $('.edit-trigger').on('click', function(){
    //   $(this).next('.menu-fast-edit').toggleClass('opened');
    // });
  }

  checkForceLoadStats() {
    if (this._forceLoadStatsChanged && (!this.userStat || !this.petStat)) {
      this.analyticsService.petStat(this.pet).then(petStat => {
        this.petStat = petStat || {};
      })
    }
  }

  isFemale() {
    return this.pet.gender === 'female';
  }

  isMale() {
    return this.pet.gender === 'male';
  }

  isSize(size) {
    return this.pet.size === size;
  }

  stat(type, pet) {
    if (this.petStat)
      return this.petStat[type] || 0;
    else {
      return this.userStat && this.userStat.pets ? 
        (this.userStat.pets[pet.id] ? this.userStat.pets[pet.id][type] || 0 : 0) : 0;
    }
  }

  open() {
    this.openClicked.emit(this.pet);
  }

  openPetEditor() {
    if (this.allowEdit) {
      this.editClicked.emit(this.pet);
      this.editMenuVisible = false;
    }
  }

  removePet(event) {
    if (this.allowEdit) {
      this.editMenuVisible = false;

      this.popup_opened = true;
      this.show_remove = true;
      if (event.stopPropagation) {
        event.stopPropagation();
      }
    }
  }

  confirmRemoval() {
    if (this.allowEdit) {
      this.globalLayoutOptionsService.setLoading(true);
      this.petService.remove(this.pet).then(() => {
        this.globalLayoutOptionsService.setLoading(false);

        this.popup_opened = false;
        this.show_remove = false;
        this.petRemoved.emit();
      }, error => {
        this.globalLayoutOptionsService.setLoading(false);
      });;
    }
  }

  cancelRemoval() {
    this.popup_opened = false;
    this.show_remove = false;
  }

  toggleEditMenu() {
    this.editMenuVisible = this.editMenuVisible ? false :  true;
    
    const component = this;
    component.menuClicked = true;
    setTimeout(() => {
      component.menuClicked = false;
    })
  }

  closeMenuIfVisible() {
    if (!this.menuClicked)
      this.editMenuVisible = false;
  }

}
