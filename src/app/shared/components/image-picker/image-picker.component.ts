import { Component, OnInit, ViewChild } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

import { UploadService } from '../../../editing/services/upload.service';

@Component({
  selector: 'app-image-picker',
  styleUrls: ['./image-picker.component.scss'],
  template: `<div [class]="containerClass">
    <span *ngIf="label && label.length > 0">{{label}}</span>
    <input #uploadFileEl type="file" [name]="inputName" [id]="inputName" (change)="fileSelected($event)">
    <label [for]="inputName" [id]="inputName">
      <span *ngIf="buttonText && !(firstInputFileUrl || fileUrlModel)">{{buttonText}}</span>
      <img *ngIf="!buttonText && !(firstInputFileUrl || fileUrlModel)" src="public/img/_icons/camera.svg" alt="Câmera" class="placeholder">
      <img *ngIf="!buttonText && (firstInputFileUrl || fileUrlModel)" [src]="firstInputFileUrl || fileUrlModel" alt="Trocar Imagem">
    </label>
  </div>`
})
export class ImagePickerComponent implements OnInit {

  @Input("containerClass") containerClass:string = 'upload-foto';
  @Input("buttonText") buttonText:string = null;
  @Input("label") label:string = '';
  @Input("inputName") inputName:string = '';
  @Input('uploadBasePath') uploadBasePath = "images";

  private _fileUrlModel:string;
  @Input()
  set fileUrlModel(fileUrlModel: string) {
    this._fileUrlModel = fileUrlModel;
    this.fileUrlModelChange.emit(this._fileUrlModel);
  }
  get fileUrlModel(): string { return this._fileUrlModel; }
  @Output() fileUrlModelChange = new EventEmitter<string>();

  @ViewChild('uploadFileEl') mainFileUpload;
  inputFiles;
  firstInputFile;
  firstInputFileUrl;

  constructor(private uploadService: UploadService) { }

  ngOnInit() {
    
  }

  fileSelected(event) {
    this.inputFiles = event.target.files;
    this.firstInputFile = event.target.files.length > 0 ? event.target.files[0] : null;
    this.firstInputFileUrl = null;

    let component = this;
    var reader = new FileReader();
    reader.onload = (readerEvent:any) => {
      component.firstInputFileUrl = readerEvent.target.result;
    }
    reader.readAsDataURL(this.firstInputFile);
  }

  hasFileSelected() : boolean {
   return this.firstInputFileUrl != null; 
  }

  uploadMainFile() : Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.inputFiles && this.inputFiles.length > 0) {
        const basePath = `${this.uploadBasePath}/${(new Date()).getTime()}`;
        this.uploadService.uploadFile(this.inputFiles[0], basePath).then(resolve, reject);
      } else {
        resolve(null);
      }
    });
  }

}
