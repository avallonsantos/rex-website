import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { SharedConfig } from '../../../shared/shared.config';
import { AuthService } from '../../../shared/services/auth.service';
import { PlaceService } from '../../../shared/services/place.service';
import { GlobalLayoutOptionsService } from '../../../shared/services/global-layout-options.service';

import { User } from '../../../global/models/user';
import { Place, PLACE_CANIL, PLACE_ONG } from '../../../global/models/place';

import { STATES } from '../../../global/models/state';
import { WEEKDAYS } from '../../../global/utils/time';
import { extendNoNulls } from '../../../global/utils/extends';

import { ImagePickerComponent } from '../image-picker/image-picker.component';
import { UserService } from '../../services/user.service';
import { addDefaultToStringList, addDefaultToList } from '../../../global/utils/lists';
import { PetService } from '../../services/pet.service';

declare const $;

@Component({
  selector: 'shared-place-form',
  templateUrl: './place-form.component.html',
  styleUrls: ['./place-form.component.scss']
})
export class PlaceFormComponent implements OnInit, OnDestroy {

  cpfMask = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  phoneMask = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
  cnpjMask = [/[0-9]/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/];

  stateList = addDefaultToList(STATES, "Estado");
  weekDays = WEEKDAYS;

  showSuccess:boolean = false;
  lastStep = 5;

  currentUser: User;
  currentUserSub: any;

  user: User;
  password: string;
  password_confirmation: string;
  
  placeholderBreed = "Raça";
  breedList = [ "Raça" ];

  private _place:Place;
  private _placeAlreadySaved:boolean;
  @Input("place")
  set place(place: Place) {
    this._place = place;
    this._placeAlreadySaved = place && place.id ? true : false;
    this.onPlaceChanged();
  }
  get place(): Place { return this._place; }

  newBreed:string = "";
  insertedTag:string = "";

  typeName:string;
  step: number = 1;
  submitted: any = { };

  @ViewChild('step1Form') step1Form;
  @ViewChild('step3Form') step3Form;
  @ViewChild('step4Form') step4Form;

  @ViewChild('selectedMainFileUpload') mainFileUpload: ImagePickerComponent;
  @ViewChild('selectedCoverFileUpload') coverFileUpload: ImagePickerComponent;

  @ViewChild('galleryPicker') galleryPicker;
  galleryNewImages = [];
  galleryBasePath = "userGalleryImages";
  
  constructor(private authService: AuthService, private placeService: PlaceService,
    private userService: UserService,
    private petService: PetService,
    private route: ActivatedRoute, private router: Router, 
    private globalLayoutOptionsService: GlobalLayoutOptionsService) { 
  }

  ngOnInit() {
    this.showSuccess = false;
    if (!SharedConfig.instance().isServer) {
      this.loadBreeds();
    }
  }

  onPlaceChanged() {
    if (!this.place)
      return;

    if (!SharedConfig.instance().isServer) {
      this.currentUserSub = this.authService.currentUserObservable.subscribe(user => {
        this.currentUser = user;
      });

      if (this.place.type == PLACE_ONG.code) {
        this.typeName = PLACE_ONG.name;
      } else if (this.place.type == PLACE_CANIL.code) {
        this.typeName = PLACE_CANIL.name;
      } else {
        this.typeName = "Empresa";
      }

      if (this.place.id) {
        this.userService.retrieveFromObject(this.place.user).then(user => {
          this.user = user ? user : new User();
          if (!this.canSeeForm()) {
            this.router.navigate(["/"]);
          }
        });
      }  else {
        this.user = new User();
      }

      this.step = 1;
    }
  }

  canSeeForm() {
    if (!this.place)
      return false;
    
    // se é user novo
    if (!this.place.id && !this.currentUser && this.user && !this.user.id)
      return true;

    if (this.place.id && this.currentUser && this.user && (this.currentUser.id == this.user.id || this.currentUser.isAdminUser()))
      return true;

    return false;
  }

  ngOnDestroy() {
    if (this.currentUserSub)
      this.currentUserSub.unsubscribe();
  }

  validateStep1() {
    return new Promise((resolve, reject) => {
      this.submitted.step_1 = true;
      if (!this.user || !this.user.id) {
        if (this.password != this.password_confirmation) {
          this.step1Form.form.errors = { password_confirmation: true };
        } else {
          this.step1Form.form.errors = null;
        }
      }
      if (!this.place.type) {
        if (this.step1Form.form.errors) {
          this.step1Form.form.errors.type = true;
        } else {
          this.step1Form.form.errors = { type: true };
        }
      }
      resolve(this.step1Form.form.valid && this.step1Form.form.errors == null);
    })
  }

  validateStep2() {
    return new Promise((resolve, reject) => {
      resolve(true);
    })
  }

  validateStep3() {
    return new Promise((resolve, reject) => {
      resolve(this.step3Form.form.valid);
    })
  }

  validateStep4() {
    return new Promise((resolve, reject) => {
      resolve(
        (this.step3Form.form.valid && this.mainFileUpload.hasFileSelected() && this.coverFileUpload.hasFileSelected())
        ||
        (this._place.main_file_url && this._place.cover_file_url));
    })
  }

  checkValidationResults(results, resolve) {
    for (let index = 0; index < results.length; index++) {
      const result = results[index];
      if (!result) {
        resolve(false)
        return false;
      }
    }
    resolve(true);
  }

  validateAllSteps() : Promise<any> {
    return new Promise((resolve, reject) => {
      Promise.all([this.validateStep1(), this.validateStep2(), this.validateStep3(), this.validateStep4()])
          .then(results => this.checkValidationResults(results, resolve), reject);
    });
  }

  validateSteps(newStep) : Promise<any> {
    return new Promise((resolve, reject) => {
      if (newStep == 2) {
        this.validateStep1().then(resolve, reject);
      } else if (newStep == 3) {
        Promise.all([this.validateStep1(), this.validateStep2()])
          .then(results => this.checkValidationResults(results, resolve), reject);
      } else if (newStep == 4) {
        Promise.all([this.validateStep1(), this.validateStep2(), this.validateStep3()])
          .then(results => this.checkValidationResults(results, resolve), reject);
      } else if (newStep >= 5) {
        Promise.all([this.validateStep1(), this.validateStep2(), this.validateStep3(), this.validateStep4()])
          .then(results => this.checkValidationResults(results, resolve), reject);
      } else {
        resolve(true);
      }
    })
    
  }

  nextStep() {
    if (this.step < 5) {
      this.goToStep(this.step+1);
    }
  }

  goToStep(step) {
    this.validateSteps(step).then(result => {
      if (result) {
        this.step = step;

        $('html, body').animate({
          scrollTop: 0
        }, 400);
      }
    });
  }

  addNewBreed() {
    if (this.newBreed && this.newBreed.length > 0) {
      this.place.breeds.push(this.newBreed);
      this.newBreed = "";
    }
  }

  tagKeyDown(event) {
    if (event.keyCode == 13 || event.keyCode == 188) {
      this.insertTag();
      if (event.preventDefault) {
        event.preventDefault();
      }
    }
  }

  insertTag() {
    let value = this.insertedTag.trim();
    if (value.length > 0) {
      this.place.tags.push(value);
      this.insertedTag = "";
    }
  }

  removeTag(tag) {
    const index = this.place.tags.indexOf(tag);
    if (index > -1) {
      this.place.tags.splice(index, 1);
    }
  }

  saveUser() : Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.currentUserSub)
        this.currentUserSub.unsubscribe();;

      if (this.currentUser) {
        this.authService.updateUserInfo(this.user).then(result => {
          resolve(this.user)
        }, reject);
      } else {
        this.authService.registerManual(this.user, this.password).then(result => {
          this.authService.updateUserInfo(this.user).then(result => {
            resolve(this.user)
          }, reject);
        }, reject);
      }
    });
  }

  registerPlace() : Promise<any> {
    const component = this;
    return new Promise((resolve, reject) => {
      const promises = [ 
        this.mainFileUpload.uploadMainFile(), 
        this.coverFileUpload.uploadMainFile(),
        this.galleryPicker.processFiles()
      ]
      Promise.all(promises).then(results => {
        if (results[0]) {
          this.place.main_file_url = results[0].url;
          this.place.main_image = extendNoNulls({}, results[0]);
        }

        if (results[1]) {
          this.place.cover_file_url = results[1].url;
          this.place.cover_image = extendNoNulls({}, results[1]);
        }

        for (const element of this.galleryNewImages) {
          if (element.uploaded)
            this.place.gallery_images.push(element.uploaded);
        }

        this.placeService.save(this.place).then(resolve, reject);
      }, error => {
        console.log(error);
        component.handleError(error);
      });
    });
  }

  loadBreeds() {
    const promises = [];
    promises.push(this.petService.breeds("dog"));
    promises.push(this.petService.breeds("cat"));
    Promise.all(promises).then(results => {
      let list = [];
      for (let index = 0; index < results.length; index++) {
        let breeds = results[index];
        for (let b = 0; b < breeds.length; b++) {
          const breed = breeds[b];
          if (list.indexOf(breed) == -1)
            list.push(breed);
        }
      }
      this.breedList = addDefaultToStringList(list, this.placeholderBreed);
    });
  }

  private handleError(error) {
    this.globalLayoutOptionsService.setLoading(false);
  }

  private saveStep() {
    this.save(this.step == this.lastStep);
  }

  save(redirect: boolean = true) {
    this.globalLayoutOptionsService.setLoading(true);

    let isNew = !this.place || !this.place.id;
    this.validateAllSteps().then(result => {
      if (result) {
        this.saveUser().then(userResult => {
          this.place.user = this.user;
          const alreadyHasOwnedPlace = this.user.ownedPlace ? true : false;
          this.registerPlace().then(placeResult => {
            if (alreadyHasOwnedPlace) {
              this.globalLayoutOptionsService.setLoading(false);
              if (redirect) {
                this.showSuccess = true;
                // this.router.navigate([this._placeAlreadySaved ? "/" : "/conta/meus-rexs"]);
              }
                
            } else {
              this.user.ownedPlace = this.place;
              this.saveUser().then(savedWithPlace => {
                this.globalLayoutOptionsService.setLoading(false);
                if (redirect) {
                  this.showSuccess = true;
                  // this.router.navigate([this._placeAlreadySaved ? "/" : "/conta/meus-rexs"]);
                }
              }, error => { this.handleError(error) });
            }
          }, error => { this.handleError(error) });
        }, error => { this.handleError(error) });
      } else {
        this.handleError(null);
      }
    }, error => { this.handleError(error) });
  }
}
