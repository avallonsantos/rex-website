import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GalleryPickerComponent } from './gallery-picker.component';

describe('GalleryPickerComponent', () => {
  let component: GalleryPickerComponent;
  let fixture: ComponentFixture<GalleryPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GalleryPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
