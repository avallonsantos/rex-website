import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';

import { UploadService } from '../../../editing/services/upload.service';

@Component({
  selector: 'app-gallery-picker',
  templateUrl: './gallery-picker.component.html',
  styleUrls: ['./gallery-picker.component.scss']
})
export class GalleryPickerComponent implements OnInit {

  @Input() currentPhotos = [];
  @Input() newPhotos = [];
  @Input() removedPhotos = [];
  @Input("label") label:string = 'Fotos';

  @Input() photoLimit:number = 10;
  
  @Input("imageClass") imageClass:string = '';
  
  @Input("addExtraClass") addExtraClass:string = '';
  @Input("addExtraText") addExtraText:string = null;
  

  @Input() basePath="galleryImageFile";

  constructor(private uploadService: UploadService) { }

  ngOnInit() {
  }

  galleryFileSelected(event) {
    const newPhotos = event.target.files;
    if (newPhotos && newPhotos.length > 0) {
      const component = this;
      let allowedCount = this.photoLimit - this.photosCount();
      for (const newPhoto of newPhotos) {
        if (allowedCount < 1)
          break;
        allowedCount = allowedCount - 1;

        var reader = new FileReader();
        reader.onload = (readerEvent:any) => {
          component.newPhotos.push({ file: newPhoto, url: readerEvent.target.result });
        }
        reader.readAsDataURL(newPhoto);
      }
    }
  }

  uploadNextFile(uploadList) : Promise<any> {
    return new Promise((resolve, reject) => {
      if (uploadList && uploadList.length > 0) {
        const uploadBasePath = `${this.basePath}/${(new Date()).getTime()}`;
        const upload = uploadList[0];
        this.uploadService.uploadFile(upload.file, uploadBasePath).then(uploadedFile => {
          upload.uploaded = uploadedFile;
          uploadList.splice(0, 1);
          this.uploadNextFile(uploadList).then(resolve, reject);
        }, reject);
      } else {
        resolve(true);
      }
    });
  }

  processFiles() : Promise<any> {
    return new Promise((resolve, reject) => {
      this.uploadNextFile(this.newPhotos.slice()).then(resolve, reject);
    });
  }

  photosCount() {
    return this.newPhotos.length + this.currentPhotos.length;
  }

}
