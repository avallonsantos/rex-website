import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

declare var $;

@Component({
  selector: 'shared-main-container',
  template: `<main style="width:100%" [class]="class" (click)="focusMainContainer()"><ng-content></ng-content></main>`
})
export class MainContainerComponent implements OnInit {

  @Input() class = "";
  @Output() focusMainContainerTriggered = new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  focusMainContainer() {
    $('#sidebar-menu').removeClass('opened');
    $('.mask-page').fadeOut('fast');

    this.focusMainContainerTriggered.emit();
  }

}
