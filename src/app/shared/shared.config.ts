export class SharedConfig {
  private static _instance = new SharedConfig();

  public isServer:boolean=false;

  static instance() {
    return SharedConfig._instance;
  }
}