import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'addressToArea'
})
export class AddressToAreaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value.neighborhood)
      return `${value.neighborhood}, ${value.city} - ${value.state}`;
    else
      return `${value.city} - ${value.state}`;
  }

}
