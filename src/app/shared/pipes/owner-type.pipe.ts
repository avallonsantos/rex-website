import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ownerType'
})
export class OwnerTypePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      return 'company';
    } else {
      return 'people';
    }
  }

}
