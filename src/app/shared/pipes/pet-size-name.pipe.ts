import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'petSizeName'
})
export class PetSizeNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value == "big") {
      return "Animal de grande porte";
    } else if (value == "medium") {
      return "Animal de médio porte";
    } else if (value == "small") {
      return "Animal de grande porte";
    } else {
      return "Animal sem porte definido";
    }
  }

}
