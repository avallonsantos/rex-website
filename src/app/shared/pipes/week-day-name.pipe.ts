import { Pipe, PipeTransform } from '@angular/core';
import { WEEKDAYS } from '../../global/utils/time';

@Pipe({
  name: 'weekDayName'
})
export class WeekDayNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return WEEKDAYS[value].name;
  }

}
