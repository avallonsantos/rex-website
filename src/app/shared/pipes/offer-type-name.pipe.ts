import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'offerTypeName'
})
export class OfferTypeNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      switch (value) {
        case "donation":
          return "Doação";
        case "sale":
          return "Venda";
        default:
          return null;
      }
    } else {
      return null;
    }
  }

}
