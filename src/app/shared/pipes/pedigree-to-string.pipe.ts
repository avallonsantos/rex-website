import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pedigreeToString'
})
export class PedigreeToStringPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value == "doc") {
      return "Sim! Pedigree documentado.";
    } else if (value == "chip") {
      return "Sim! Sou microchipado.";
    } else if (value == "false") {
      return "Não";
    }
  }

}
