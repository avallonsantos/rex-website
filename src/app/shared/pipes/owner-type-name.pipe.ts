import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ownerTypeName'
})
export class OwnerTypeNamePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (value) {
      switch (value) {
        case "ong":
          return "ONG";
        case "canil":
          return "Canil";
        default:
          return "Empresa";
      }
    } else {
      return 'Pessoa';
    }
  }

}
