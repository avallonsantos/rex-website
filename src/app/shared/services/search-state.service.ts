import { Injectable, OnInit } from '@angular/core';
import { EventEmitter } from '@angular/core';

import { SearchData } from './pet.service';
import { SharedConfig } from '../shared.config'

import { Subject, ReplaySubject } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SearchStateService implements OnInit{

  private _search: SearchData;
  private _searchSubject: Subject<SearchData> = new ReplaySubject();
  public readonly searchObservable: Observable<any> = this._searchSubject.asObservable();

  constructor() {
    if (!SharedConfig.instance().isServer) {
      if (!this._search) {
        this.setSearch({ 
          type: { dog: false, cat: false },
          place_type: { canil: false, ong: false },
          breed: null,
          age_range: null,
          size: null,
          place_state: null,
        });
      }
    } else {
      this.setSearch({ 
        type: { dog: false, cat: false },
        place_type: { canil: false, ong: false },
        breed: null,
        age_range: null,
        size: null,
        place_state: null,
      });
    }
   }

  ngOnInit() {
    
  }

  setSearch(search: SearchData) {
    this._search = search;
    this._searchSubject.next(search);
  }

}
