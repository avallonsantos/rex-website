import { TestBed, inject } from '@angular/core/testing';

import { GlobalLayoutOptionsService } from './global-layout-options.service';

describe('GlobalLoadingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalLayoutOptionsService]
    });
  });

  it('should be created', inject([GlobalLayoutOptionsService], (service: GlobalLayoutOptionsService) => {
    expect(service).toBeTruthy();
  }));
});
