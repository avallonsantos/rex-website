import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';
import { AngularFirestore, AssociatedReference } from 'angularfire2/firestore';
import { DocumentReference } from '@firebase/firestore-types';

import { extendNoNulls } from '../../global/utils/extends';
import { User } from '../../global/models/user';

@Injectable()
export class UserService {

  private basePath = "users";

  constructor(private firestore: AngularFirestore) { }

  retrieveFromObject(item) : Promise<User> {
    return new Promise((resolve, reject) => {
      if (item && item.constructor.name == "DocumentReference") {
        this.getByRef(item).then(resolve, reject);
      } else if (typeof item === 'string') {
        this.get(item).then(resolve, reject);
      } else if (item instanceof User) {
        resolve(item);
      } else if (Object.keys(item).length > 1 && item.id) {
        resolve(new User(item.id, item));
      } else {
        resolve(null);
      }
    });
  }

  get(userId) {
    return this.getByPath(`${this.basePath}/${userId}`);
  }

  search(search) : Promise<User[]> {
    return new Promise((resolve, reject) => {
      let searchRef = this.firestore.collection<User>(this.basePath).ref;
      
      searchRef.get().then(resultData => {
        const results:User[] = [];
        for (let index = 0; index < resultData.docs.length; index++) {
          let item = resultData.docs[index];
          results.push(new User(item.id, item.data()));
        }
        resolve(results);
      }, reject)
    });
  }

  private getByRef(docRef: any) {
    return new Promise((resolve, reject) => {
      docRef.get().then(doc => {
        if (doc.exists) {
          resolve(new User(docRef.id, doc.data()));
        } else {
          resolve(null);
        }
      }, reject)
    });
  }

  private getByPath(path) {
    return new Promise((resolve, reject) => {
      this.firestore.doc(path).snapshotChanges().take(1).subscribe(result => {
        if (result.payload.exists) {
          resolve(new User(result.payload.id, result.payload.data()));
        } else {
          resolve(null);
        }
      });
    });
  }

}
