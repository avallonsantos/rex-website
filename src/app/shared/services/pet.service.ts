import { Injectable, group } from '@angular/core';

import * as firebase from 'firebase/app';
import { AngularFirestore } from 'angularfire2/firestore';

import { extendNoNulls } from '../../global/utils/extends';
import { Pet } from '../../global/models/pet';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import { getIdFromSlugified } from '../../global/utils/string';

@Injectable()
export class PetService {

  petTypesMap: { [key:string]:any } = {};

  constructor(private firestore: AngularFirestore) {
    
   }

  save(pet: Pet) {
    return new Promise((resolve, reject) => {
      const firestoreData = pet.toFirestore(this.firestore);
      if (pet.id) {
        const dbRef = this.firestore.doc<Pet>(`pets/${pet.id}`);
        dbRef.update(firestoreData).then(() => {
          resolve(pet);
        }, reject);
      } else {
        const dbRef = this.firestore.collection('pets');
        firestoreData.createdAt = new Date();
        dbRef.add(firestoreData).then(petRef => {
          pet.id = petRef.id;
          resolve(pet);
        }, reject);
      }
    });
  }

  remove(pet: Pet) {
    return new Promise((resolve, reject) => {
      if (pet.id) {
        const dbRef = this.firestore.doc<Pet>(`pets/${pet.id}`);
        dbRef.delete().then(() => {
          resolve(pet);
        }, reject);
      } else {
        resolve(pet);
      }
    });
  }

  retrieveFromObject(item) {
    return new Promise((resolve, reject) => {
      if (item && item.constructor.name == "DocumentReference") {
        this.getByRef(item).then(resolve, reject);
      } else if (typeof item === 'string') {
        this.get(item).then(resolve, reject);
      } else if (item instanceof Pet) {
        resolve(item);
      } else if (Object.keys(item).length > 1 && item.id) {
        resolve(new Pet(item.id, item));
      } else {
        resolve(null);
      }
      
    });
  }

  getBySlug(slug) : Promise<Pet> {
    return new Promise((resolve, reject) => {
      let searchRef = this.firestore.collection<Pet>('pets').ref;
      let query:firebase.firestore.Query = searchRef.where("slug", "==", slug);
      
      query.get().then(resultData => {
        if (resultData.empty) {
          return this.get(getIdFromSlugified(slug)).then(resolve, reject);
        } else {
          let item = resultData.docs[0];
          resolve(new Pet(item.id, item.data()));
        }
      }, reject)
    });
  }

  get(petId) {
    return this.getByPath(`pets/${petId}`);
  }

  private getByPath(path) {
    return new Promise((resolve, reject) => {
      this.firestore.doc(path).snapshotChanges().take(1).subscribe(result => {
        if (result.payload.exists) {
          resolve(new Pet(result.payload.id, result.payload.data()));
        } else {
          resolve(null);
        }
      }, error => {
        console.error(error);
        reject(error);
      });
    });
  }

  searchPets(searchInfo: any) : Promise<Pet[]> {
    const searchData = searchInfo as SearchData;
    return new Promise((resolve, reject) => {
      let searchRef = this.firestore.collection<Pet>('pets').ref;
      let query: any = null;
      if (!searchData.type.dog || !searchData.type.cat) {
        let type = searchData.type.dog ? "dog" : (searchData.type.cat ? "cat" : null);
        if (type)
          query = searchRef.where("type", "==", type)
      }
      if (searchData.breed && searchData.breed !== "-" && searchData.breed.length > 0) {
        query = (query || searchRef).where("breed", "==", searchData.breed)
      }
      if (searchData.age_range) {
        const age_splitted = searchData.age_range.split("-");

        const beginAge = parseInt(age_splitted[0]);
        const beginDate = new Date();
        beginDate.setDate(1);
        beginDate.setHours(0, 0, 0, 0);
        beginDate.setFullYear(beginDate.getFullYear() - beginAge);

        const endAge = parseInt(age_splitted[1]);
        const endDate = new Date();
        endDate.setDate(1);
        endDate.setHours(0, 0, 0, 0);
        endDate.setFullYear(endDate.getFullYear() - endAge);

        console.log('beginDate', beginDate);
        console.log('endDate', endDate);

        query = (query || searchRef).where("bornAt", "<=", beginDate).where("bornAt", ">", endDate);
      }
      if (searchData.size) {
        query = (query || searchRef).where("size", "==", searchData.size)
      }
      if (searchData.place_state) {
        query = (query || searchRef).where("place_address.state", "==", searchData.place_state)
      }
      if (!searchData.place_type.canil || !searchData.place_type.ong) {
        let place_type = searchData.place_type.canil ? "canil" : (searchData.place_type.ong ? "ong" : null);
        if (place_type)
          query = (query || searchRef).where("place_type", "==", place_type)
      }
      
      if (query) {
        query.get().then(resultData => {
          const results = [];
          for (let index = 0; index < resultData.docs.length; index++) {
            let item = resultData.docs[index];
            results.push(new Pet(item.id, item.data()));
          }
          resolve(results);
        }, error => {
          console.error(error);
          reject(error);
        })
      } else {
        this.loadFeaturedPets().then(resolve, reject);
      }
    });
  }

  loadMyPets(user) : Promise<Pet[]>{
    return new Promise((resolve, reject) => {
      const userRef = this.firestore.doc(`users/${user.id}`).ref;
      let searchRef = this.firestore.collection<Pet>('pets').ref;
      let query:firebase.firestore.Query = searchRef.where("user", "==", userRef);
      
      query.get().then(resultData => {
        const results = [];
        for (let index = 0; index < resultData.docs.length; index++) {
          let item = resultData.docs[index];
          results.push(new Pet(item.id, item.data()));
        }
        resolve(results);
      }, reject)
    });
  }

  loadPlacePets(place) : Promise<Pet[]>{
    return new Promise((resolve, reject) => {
      const placeRef = this.firestore.doc(`places/${place.id}`).ref;
      let searchRef = this.firestore.collection<Pet>('pets').ref;
      let query:firebase.firestore.Query = searchRef.where("place", "==", placeRef);
      
      query.get().then(resultData => {
        const results = [];
        for (let index = 0; index < resultData.docs.length; index++) {
          let item = resultData.docs[index];
          results.push(new Pet(item.id, item.data()));
        }
        resolve(results);
      }, reject)
    });
  }

  loadFeaturedPets() : Promise<Pet[]> {
    return new Promise((resolve, reject) => {
      this.firestore.collection<Pet>('pets').snapshotChanges().take(1).subscribe(items => {
        const results = [];
        items.forEach(item => {
          results.push(new Pet(item.payload.doc.id, item.payload.doc.data()));
        });
        resolve(results);
      }, reject);
    });
  }
  
  groupResults(results: Pet[], groupSize:number) {
    return groupList<Pet>(results, groupSize);
  }

  breeds(type: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.petTypesMap[type]) {
        resolve(this.petTypesMap[type].breeds);
      } else {
        this.firestore.doc(`pet_types/${type}`).snapshotChanges().take(1).subscribe(petType => {
          let data = petType.payload.data();
          this.petTypesMap[type] = data;
          data.breeds.sort();
          data.usedBreeds.sort();
          resolve(data.breeds);
        }, reject);
      }
    });
  }

  cachedUsedBreeds(type: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.petTypesMap[type]) {
        resolve(this.petTypesMap[type].usedBreeds);
      } else {
        this.usedBreeds(type);
      }
    });
  }

  usedBreeds(type: string) : Promise<any> {
    return new Promise((resolve, reject) => {
      this.firestore.doc(`pet_types/${type}`).snapshotChanges().take(1).subscribe(petType => {
        let data = petType.payload.data();
        this.petTypesMap[type] = data;
        data.breeds.sort();
        data.usedBreeds.sort();
        resolve(data.usedBreeds);
      }, reject);
    });
  }

  private getByRef(docRef) {
    return new Promise((resolve, reject) => {
      docRef.get().then(doc => {
        if (doc.exists) {
          resolve(new Pet(docRef.id, doc.data()));
        } else {
          resolve(null);
        }
      }, reject)
    });
  }
}

interface SearchDataType {
  dog: boolean;
  cat: boolean;
}
interface SearchDataLocalType {
  canil: boolean;
  ong: boolean;
}
export interface SearchData {
  type: SearchDataType;
  place_type: SearchDataLocalType;
  breed:string;
  age_range:string; // START-END (ex: 0-1, 1-3)
  size:Number;
  place_state:string;
}

import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injector } from '@angular/core';
import { SharedConfig } from '../shared.config';
import { groupList } from '../../global/utils/lists';

@Injectable()
export class PetResolver implements Resolve<Pet> {
  constructor(private petService: PetService,
    private router: Router,
    private injector: Injector) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    let injectedItem = null;
    if (SharedConfig.instance().isServer) {
      injectedItem = this.injector.get('pet', null);
      console.log('pet resolved', injectedItem);
    }
    if (injectedItem) {
      let pet = new Pet(injectedItem.id, injectedItem);
      console.log('pet resolved', pet);
      return pet;
    } else {
      let slug = route.paramMap.get('slug');
      return this.petService.getBySlug(slug).then(item => {
        if (item) {
          return item;
        } else { // id not found
          this.router.navigate(['/']);
          return null;
        }
      });
    }
  }
}