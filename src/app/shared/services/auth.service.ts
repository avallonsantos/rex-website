import { Injectable, OnDestroy, EventEmitter } from '@angular/core';
import { User } from '../../global/models/user';

import { Subject, ReplaySubject } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase/app';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';

import { SharedConfig } from '../shared.config';

@Injectable()
export class AuthService implements OnDestroy {

  currentUser: User;
  private _currentUserSubject: Subject<User> = new ReplaySubject();
  public readonly currentUserObservable: Observable<any> = this._currentUserSubject.asObservable();
  
  currentAuthUser: any;
  authObserver: any;

  loginNavParams: any[];
  loginRequested: EventEmitter<any[]> = new EventEmitter;

  constructor(public af: AngularFireAuth,
    private firestore: AngularFirestore) { 
      
    if (!SharedConfig.instance().isServer) {
      this.init();
    }
  }

  requestLogin(navParams) {
    this.loginNavParams = navParams;
    this.loginRequested.emit(navParams);
  }

  clearLoginNavParams() {
    this.loginNavParams = null;
  }

  init() {
    this.authObserver = this.af.authState.subscribe( authUser => {
      if (authUser) {
        this.retrieveUser(authUser);
      } else {
        this.setUserAndAuth(null, null);
      }
    });
  }

  private retrieveUser(authUser) {
    return new Promise((resolve, reject) => {
      if (this.currentUser && this.currentUser.id == authUser.uid) {
        resolve(this.currentUser);
      } else {
        this.firestore.doc(`users/${authUser.uid}`).snapshotChanges().take(1).subscribe(result => {
          if (result.payload.exists) {
            const user = new User(result.payload.id, result.payload.data());
            this.setUserAndAuth(user, authUser);
            resolve(user);
          } else {
            this.setUserAndAuth(null, authUser);
            resolve(null);
          }
        });
      }
    });
  }

  ngOnDestroy() {
    if (this.authObserver)
      this.authObserver.unsubscribe();
  }

  private setUserAndAuth(user: User, auth: any) {
    this.currentAuthUser = auth;
    this.currentUser = user;
    this._currentUserSubject.next(user);
  }

  private saveUser(user: User) : Promise<User> {
    return new Promise<User>((resolve, reject) => {
      const firestoreData = user.toFirestore(this.firestore);
      if (user.id) {
        const dbRef = this.firestore.doc<User>(`users/${user.id}`);
        dbRef.set(firestoreData).then(() => {
          resolve(user);
        }, reject);
      } else {
        const dbRef = this.firestore.collection('users');
        dbRef.add(firestoreData).then(userRef => {
          user.id = userRef.id;
          resolve(user);
        }, reject);
      }
    });
  }

  isLogged() {
    return this.currentUser;
  }

  requiresRegistration() {
    return !this.currentAuthUser || !this.currentUser;
  }

  logout() : Promise<any> {
    return new Promise((resolve, reject) => {
      this.af.auth.signOut().then(() => {
        this.setUserAndAuth(null, null);
        resolve();
      }, reject)
    });
  }

  loginWithFacebook() : Promise<User> {
    return new Promise<User>((resolve, reject) => {
      return this.af.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(authInfo => {
        this.retrieveUser(authInfo.user || authInfo).then(resolve, reject);
      }, reject);
    });
  }

  loginWithGoogle() : Promise<User> {
    return new Promise<User>((resolve, reject) => {
      this.af.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(authInfo => {
        this.retrieveUser(authInfo.user || authInfo).then(resolve, reject);
      }, reject);
    });
  }

  loginManual(email: string, password: string) : Promise<User> {
    return new Promise<User>((resolve, reject) => {
      this.af.auth.signInWithEmailAndPassword(email, password).then(authInfo => {
        this.retrieveUser(authInfo.user || authInfo).then(resolve, reject);
      }, reject);
    });
  }

  private hasProvider(authInfo, providerId) {
    for (let i = 0; i < authInfo.providerData.length; i++) {
      let providerInfo = authInfo.providerData[i];
      if (providerInfo.providerId === providerId) {
        return true;
      }
    }
    return false;
  }

  private reauthenticate(authInfo) {
    return this.af.auth.currentUser.getIdToken();
  }

  registerManual(user: User, password: string) : Promise<User> {
    return new Promise<User>((resolve, reject) => {
      if (this.currentAuthUser && this.af.auth.currentUser) {
        if (this.hasProvider(this.currentAuthUser, firebase.auth.EmailAuthProvider.PROVIDER_ID)) {
          this.setUserAndAuth(user, this.currentAuthUser);
          resolve(user);
        } else {
          this.reauthenticate(this.currentAuthUser).then(newAuth => {
            var credential = firebase.auth.EmailAuthProvider.credential(user.email, password);
            this.af.auth.currentUser.linkWithCredential(credential).then(snap => {
              this.setUserAndAuth(user, snap.user || snap);
              resolve(user);
            }, reject);
          }, reject);
        }
      } else {
        this.af.auth.createUserWithEmailAndPassword(user.email, password).then(snap => {
          let userUid = snap.$key || snap.uid;
          user.id = userUid;
          this.saveUser(user).then(userData => { 
            this.setUserAndAuth(user, snap);
            resolve(user);
          }, reject);
        }, reject);
      }
    });
  }

  updateUserInfo(user: User): Promise<User> {
    return this.saveUser(user);
  }

}
