import { Injectable } from '@angular/core';

import { Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GlobalLayoutOptionsService {

  private _isLoading: boolean = false;
  private _isLoadingSubject: Subject<boolean> = new ReplaySubject();
  public readonly isLoadingObservable: Observable<any> = this._isLoadingSubject.asObservable();

  private _maskPageVisibility: boolean = false;
  private _maskPageVisibilitySubject: Subject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly maskPageVisibilityObservable: Observable<boolean> = this._maskPageVisibilitySubject.asObservable();

  private _mobileSearchVisibility: boolean = false;
  private _mobileSearchVisibilitySubject: Subject<boolean> = new ReplaySubject();
  public readonly mobileSearchVisibilityObservable: Observable<any> = this._mobileSearchVisibilitySubject.asObservable();

  private _mobileMenuAnchorSearchVisibility: boolean = false;
  private _mobileMenuAnchorSearchVisibilitySubject: Subject<boolean> = new ReplaySubject();
  public readonly mobileMenuAnchorSearchVisibilityObservable: Observable<any> = this._mobileMenuAnchorSearchVisibilitySubject.asObservable();

  constructor() {
    this.setLoading(false);
    this.setMaskPageVisibility(false);
  }

  setMobileSearchVisibility(componentVisibility, menuVisibility = true) {
    if (this._mobileSearchVisibility != componentVisibility) {
      this._mobileSearchVisibility = componentVisibility;
      this._mobileSearchVisibilitySubject.next(componentVisibility);
    }

    if (this._mobileMenuAnchorSearchVisibility != menuVisibility) {
      this._mobileMenuAnchorSearchVisibility = menuVisibility;
      this._mobileMenuAnchorSearchVisibilitySubject.next(menuVisibility);
    }
  }

  // toggleMobileSearchVisibility() {
  //   this.setMobileSearchVisibility(this._mobileSearchVisibility ? false : true);
  // }

  mobileSearchVisibility() {
    return this._mobileSearchVisibility;
  }

  setMaskPageVisibility(visible: boolean) {
    if (this._maskPageVisibility != visible) {
      this._maskPageVisibility = visible;
      this._maskPageVisibilitySubject.next(visible);
    }
  }

  setLoading(loading: boolean) {
    this._isLoading = loading;
    this._isLoadingSubject.next(loading);
  }

  isLoading() {
    return this._isLoading;
  }

}
