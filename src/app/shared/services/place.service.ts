import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';
import { AngularFirestore } from 'angularfire2/firestore';

import { Place } from '../../global/models/place';
import { User } from '../../global/models/user';
import { getIdFromSlugified } from '../../global/utils/string';

@Injectable()
export class PlaceService {

  private basePath = "places";

  constructor(private firestore: AngularFirestore) { }

  save(item: Place) {
    return new Promise((resolve, reject) => {
      const firestoreData = item.toFirestore(this.firestore);
      if (item.id) {
        const dbRef = this.firestore.doc<Place>(`${this.basePath}/${item.id}`);
        dbRef.set(firestoreData).then(() => {
          resolve(item);
        }, reject);
      } else {
        const dbRef = this.firestore.collection(this.basePath);
        dbRef.add(firestoreData).then(itemRef => {
          item.id = itemRef.id;
          resolve(item);
        }, reject);
      }
    });
  }

  getBySlug(slug) : Promise<Place> {
    return new Promise((resolve, reject) => {
      let searchRef = this.firestore.collection<Place>('places').ref;
      let query:firebase.firestore.Query = searchRef.where("slug", "==", slug);
      
      query.get().then(resultData => {
        if (resultData.empty) {
          return this.get(getIdFromSlugified(slug)).then(resolve, reject);
        } else {
          let item = resultData.docs[0];
          resolve(new Place(item.id, item.data()));
        }
      }, reject)
    });
  }

  get(placeId) {
    return this.getByPath(`${this.basePath}/${placeId}`);
  }

  search(search) : Promise<Place[]> {
    return new Promise((resolve, reject) => {
      let searchRef = this.firestore.collection<Place>(this.basePath).ref;
      
      searchRef.get().then(resultData => {
        const results:Place[] = [];
        for (let index = 0; index < resultData.docs.length; index++) {
          let item = resultData.docs[index];
          results.push(new Place(item.id, item.data()));
        }
        resolve(results);
      }, reject)
    });
  }

  listByUser(user) : Promise<Place[]> {
    return new Promise((resolve, reject) => {
      const userRef = this.firestore.doc(`users/${user.id}`).ref;
      let searchRef = this.firestore.collection<Place>(this.basePath).ref;
      let query:firebase.firestore.Query = searchRef.where("user", "==", userRef);
      
      query.get().then(resultData => {
        const results:Place[] = [];
        for (let index = 0; index < resultData.docs.length; index++) {
          let item = resultData.docs[index];
          results.push(new Place(item.id, item.data()));
        }
        resolve(results);
      }, reject)
    });
  }

  retrieveFromObject(item) : Promise<Place> {
    return new Promise((resolve, reject) => {
      if (item && item.constructor.name == "DocumentReference") {
        this.getByRef(item).then(resolve, reject);
      } else if (typeof item === 'string') {
        this.get(item).then(resolve, reject);
      } else if (item instanceof Place) {
        resolve(item);
      } else if (Object.keys(item).length > 1 && item.id) {
        resolve(new Place(item.id, item));
      } else {
        resolve(null);
      }
      
    });
  }

  private getByRef(docRef) {
    return new Promise((resolve, reject) => {
      docRef.get().then(doc => {
        if (doc.exists) {
          resolve(new Place(docRef.id, doc.data()));
        } else {
          resolve(null);
        }
      }, reject)
    });
  }

  private getByPath(path) {
    return new Promise((resolve, reject) => {
      this.firestore.doc(path).snapshotChanges().take(1).subscribe(result => {
        if (result.payload.exists) {
          resolve(new Place(result.payload.id, result.payload.data()));
        } else {
          resolve(null);
        }
      });
    });
  }

}

import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { SharedConfig } from '../shared.config';
import { Injector } from '@angular/core';

@Injectable()
export class PlaceResolver implements Resolve<Place> {
  constructor(private placeService: PlaceService,
    private router: Router,
    private injector: Injector) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    const injectedItem = SharedConfig.instance().isServer ? this.injector.get('place', null) : null;
    if (injectedItem) {
      return new Place(injectedItem.id, injectedItem);
    } else {
      let slug = route.paramMap.get('slug');
      return this.placeService.getBySlug(slug).then(place => {
        if (place) {
          return place;
        } else { // id not found
          this.router.navigate(['/']);
          return null;
        }
      });
    }
  }
}
