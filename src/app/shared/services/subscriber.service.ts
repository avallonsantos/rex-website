import { Injectable } from '@angular/core';
import { NewsletterSubscriber } from '../../global/models/newsletter-subscriber';

import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class SubscriberService {

  constructor(private firestore: AngularFirestore) { }

  subscribe(item: NewsletterSubscriber) {
    return new Promise((resolve, reject) => {
      const firestoreData = item.toFirestore(this.firestore);
      if (item.id) {
        const dbRef = this.firestore.doc<NewsletterSubscriber>(`newsletterSubscribers/${item.id}`);
        dbRef.update(firestoreData).then(() => {
          resolve(item);
        }, reject);
      } else {
        firestoreData.createdAt = new Date();
        const dbRef = this.firestore.collection('newsletterSubscribers');
        dbRef.add(firestoreData).then(itemRef => {
          item.id = itemRef.id;
          resolve(item);
        }, reject);
      }
    });
  }

  remove(item: NewsletterSubscriber) {
    return new Promise((resolve, reject) => {
      if (item.id) {
        const dbRef = this.firestore.doc<NewsletterSubscriber>(`newsletterSubscribers/${item.id}`);
        dbRef.delete().then(() => {
          resolve(item);
        }, reject);
      } else {
        resolve(item);
      }
    });
  }

  searchSubscribers(email: string) : Promise<NewsletterSubscriber[]> {
    return new Promise((resolve, reject) => {
      let searchRef = this.firestore.collection<NewsletterSubscriber>('newsletterSubscribers').ref;
      searchRef.where("email", "==", email).get().then(resultData => {
        const results = [];
        for (let index = 0; index < resultData.docs.length; index++) {
          let item = resultData.docs[index];
          results.push(new NewsletterSubscriber(item.id, item.data()));
        }
        resolve(results);
      }, error => {
        console.error(error);
        reject(error);
      });
    });
  }

  loadAll() : Promise<NewsletterSubscriber[]>{
    return new Promise((resolve, reject) => {
      this.firestore.collection<NewsletterSubscriber>('newsletterSubscribers').ref.get().then(resultData => {
        const results = [];
        for (let index = 0; index < resultData.docs.length; index++) {
          let item = resultData.docs[index];
          results.push(new NewsletterSubscriber(item.id, item.data()));
        }
        resolve(results);
      }, reject)
    });
  }

}
