import { Injectable, OnInit, OnDestroy } from '@angular/core';

import { Subject, ReplaySubject } from 'rxjs';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase/app';
import { AngularFirestore } from 'angularfire2/firestore';

import { IUserStats, IPetStats } from '../../global/models/analytics';
import { AuthService } from './auth.service';
import { Http } from '@angular/http';

@Injectable()
export class AnalyticsService implements OnInit, OnDestroy {

  private _currentUserStat: IUserStats;
  private _currentUserStatSubject: Subject<IUserStats> = new ReplaySubject();
  public readonly currentUserStatSubjectObservable: Observable<any> = this._currentUserStatSubject.asObservable();

  private authSub;
  private currentUserStatSub;

  constructor(private firestore: AngularFirestore,
    private authService: AuthService,
    private http: Http) { 
  
    this.authSub = this.authService.currentUserObservable.subscribe(user => {
      this.checkUser(user);
    });
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {
    if (this.authSub)
      this.authSub.unsubscribe();

    this.unsubscribeStat();
  }

  checkUser(user) {
    this.unsubscribeStat();
    if (user) {
      this.currentUserStatSub = this.firestore.doc<IUserStats>(`usersStats/${user.id}`).snapshotChanges().subscribe(result => {
        if (result.payload.exists) {
          this.setCurrentUserStat(result.payload.data());
        } else {
          this.setCurrentUserStat(null);
        }
      })
    }
  }

  petStat(pet) {
    return new Promise((resolve, reject) => {
      this.firestore.doc<IPetStats>(`petsStats/${pet.id}`).snapshotChanges().take(1).subscribe(result => {
        if (result.payload.exists) {
          resolve(result.payload.data());
        } else {
          resolve(null);
        }
      })
    });
  }

  unsubscribeStat() {
    if (this.currentUserStatSub)
      this.currentUserStatSub.unsubscribe();
  }

  private setCurrentUserStat(userStat) {
    this._currentUserStat = userStat;
    this._currentUserStatSubject.next(userStat);
  }

  saveInterest(pet, currentUser, interest_type) {
    return new Promise(async (resolve, reject) => {
      const userRef = this.firestore.doc(`users/${currentUser.id}`).ref;
      const petRef = this.firestore.doc(`pets/${pet.id}`).ref;
      let searchRef = this.firestore.collection('interests').ref;
      let query:firebase.firestore.Query = searchRef.where("user", "==", userRef).where("pet", "==", petRef);
      
      query.get().then(resultData => {
        if (resultData.empty) {
          const interest: any = {
            pet: this.firestore.doc(`pets/${pet.id}`).ref,
            user: this.firestore.doc(`users/${currentUser.id}`).ref,
            type: interest_type,
            createdAt: (new Date()),
          }
          this.firestore.collection('interests').add(interest).then(interestRef => {
            interest.id = interestRef.id;
            resolve(interest);
          }, reject);
        } else {
          resolve(resultData.docs[0].data());
        }
      }, reject);
    });
  }

  visitPet(pet) {
    return new Promise(async (resolve, reject) => {
      try {
        const baseUrl = "https://us-central1-rex-website-live.cloudfunctions.net/pub";
        const url = `${baseUrl}/pet/visit`;
        this.http.post(url, { petId: pet.id}).map(res => res.json()).subscribe(result => {
          resolve(result);
        }, error => {
          console.error(error);
          reject(error);
        });
      } catch (ex) {
        console.error(ex);
        reject(ex);
      }
    });
  }

}
