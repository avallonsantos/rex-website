import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

export interface ContactData {
  email: string;
  displayName: string;
  subject:string;
  message:string;
}

@Injectable()
export class ContactService {

  constructor(private http: Http) { }

  sendContact(contactData: ContactData) {
    return new Promise(async (resolve, reject) => {
      try {
        const baseUrl = "https://us-central1-rex-website-live.cloudfunctions.net/pub";
        const url = `${baseUrl}/contact`;
        this.http.post(url, contactData).map(res => res.json()).subscribe(result => {
          resolve(result);
        }, error => {
          console.error(error);
          reject(error);
        });
      } catch (ex) {
        console.error(ex);
        reject(ex);
      }
    });
  }
}
