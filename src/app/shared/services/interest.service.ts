import { Injectable } from '@angular/core';
import { Interest } from '../../global/models/interest';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class InterestService {

  private basePath = "interests";

  constructor(private firestore: AngularFirestore) { }

  search(search) : Promise<Interest[]> {
    return new Promise((resolve, reject) => {
      let searchRef = this.firestore.collection<Interest>(this.basePath).ref;
      
      searchRef.get().then(resultData => {
        const results:Interest[] = [];
        for (let index = 0; index < resultData.docs.length; index++) {
          let item = resultData.docs[index];
          results.push(new Interest(item.id, item.data()));
        }
        resolve(results);
      }, reject)
    });
  }

  rate(interest: Interest, rating: number) {
    return new Promise((resolve, reject) => {
      if (interest.rating && interest.rating > -1) {
        reject("Already rated");
      } else {
        const dbRef = this.firestore.doc(`${this.basePath}/${interest.id}`);
        dbRef.update({ rating: rating }).then(petRef => {
          interest.rating = rating;
          resolve(interest);
        }, reject);
      }
    });
  }
}
