import { Component, OnInit, OnDestroy, HostListener, Inject, HostBinding } from '@angular/core';
import { trigger, state, animate, transition, style } from '@angular/animations';
import { Router, NavigationEnd } from '@angular/router';

import { PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { SharedConfig } from './shared/shared.config';

import { User } from './global/models/user';
import { AuthService } from './shared/services/auth.service';
import { Observable } from 'rxjs/Observable';
import { GlobalLayoutOptionsService } from './shared/services/global-layout-options.service';

declare const $;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('pageMaskOpacity', [
      state('shown' , style({ opacity: 1 })),
      state('hidden', style({ opacity: 0 })),
      transition('* <=> *', animate('200ms ease'))
      
    ]),
    trigger('pageMaskDisplay', [
      state('shown' , style({ display: "block" })),
      state('hidden', style({ display: "none" })),
      transition('hidden => shown', animate('0ms ease')),
      transition('shown => hidden', animate('0ms 200ms ease'))
    ])
  ],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'Rex';
  sidebarVisible = false;
  loginVisible = false;
  loginVisibleSub;

  loadingVisible: boolean = false;
  loadingVisibleSub;

  pageMaskVisibility = 'hidden';
  pageMaskVisibilitySub;
  isServer = false;

  currentUser: User;
  currentUserSub: any;

  navigationSubscription;
  
  @HostBinding('@.disabled') public animationsDisabled = false;
  

  constructor(@Inject(PLATFORM_ID) private platformId: Object,
    private authService: AuthService,
    private router: Router,
    private globalLayoutOptionsService: GlobalLayoutOptionsService) { 
    if (isPlatformServer(this.platformId)) {
      SharedConfig.instance().isServer = true;
      this.isServer = true;
      this.animationsDisabled = true;
    } else {
      this.loadingVisibleSub = this.globalLayoutOptionsService.isLoadingObservable.subscribe(loading => {
        this.setLoading(loading);
      });

      this.currentUserSub = this.authService.currentUserObservable.subscribe(user => {
        this.currentUser = user;
      });

      this.authService.loginRequested.subscribe(lastPageRequested => {
        this.loginVisible = true;
      })
    }
  }

  ngOnInit() {
    if (!SharedConfig.instance().isServer) {
      this.navigationSubscription = this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
            return;
        }
        window.scrollTo(0, 0);
      });

      this.pageMaskVisibilitySub = this.globalLayoutOptionsService.maskPageVisibilityObservable.subscribe(visible => {
        this.pageMaskVisibility = visible ? 'shown' : 'hidden';
      });

    }
  }

  ngOnDestroy() {
    if (this.currentUserSub)
      this.currentUserSub.unsubscribe();

    if (this.loginVisibleSub)
      this.loginVisibleSub.unsubscribe();
    
    if (this.loadingVisibleSub)
      this.loadingVisibleSub.unsubscribe();

    if (this.pageMaskVisibilitySub)
      this.pageMaskVisibilitySub.unsubscribe();

    if (this.navigationSubscription)
      this.navigationSubscription.unsubscribe();
  }
  
  sidebarShown() {
    if (this.pageMaskVisibility != 'shown')
      this.pageMaskVisibility = 'shown'
  }
  sidebarHidden() {
    if (this.pageMaskVisibility != 'hidden')
      this.pageMaskVisibility = 'hidden';
  }

  setLoading(loadingVisible) {
    this.loadingVisible = loadingVisible;
    
    let body = $('body');
    if (this.loadingVisible) {
      if (!body.hasClass('loading'))
        body.addClass('loading');
    } else {
      if (body.hasClass('loading'))
        body.removeClass('loading');
    }
  }

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    if (event.keyCode == 27) { 
      this.sidebarVisible = false;
    }
  }

  hideSidebar() {
    this.sidebarVisible = false;
  }

  showLogin() {
    this.loginVisible = true;
    this.pageMaskVisibility = 'shown';
  }

  onLoginExit() {
    this.pageMaskVisibility = 'hidden';
  }

  onLogin() {
    if (this.router.url == "/registrar") {
      this.router.navigate(this.authService.loginNavParams ? this.authService.loginNavParams : ["/"]);
      this.authService.clearLoginNavParams();
    } 

    if (this.authService.requiresRegistration()) {
      this.router.navigate(["/registrar"]);
    }
  }
}
